﻿namespace LRI.MyPlace2Learn.Models
{
    public class OrderDetail
    {
        public int OrderDetailId { get; set; }
        public int OrderId { get; set; }
        public int LearningResourceId { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public System.DateTime ExpiryDate { get; set; }

        public virtual LearningResource LearningResource { get; set; }
        public virtual Order Order { get; set; }

        

    }
}
