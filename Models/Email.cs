﻿extern alias DataAnnotations;
using System.ComponentModel;
using DataAnnoSchema = DataAnnotations.System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Web.Mvc;

namespace LRI.MyPlace2Learn.Models
{
    [Bind(Exclude = "EmailId")]
    public class Email
    {

        public int EmailId { get; set; }
        public string Reference { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }
        public bool HasSender { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedByUserId { get; set; }
        public System.DateTime? UpdatedOn { get; set; }
        public int? UpdatedByUserId { get; set; }

		[DataAnnoSchema.NotMapped]
        public string To { get; set; }
		[DataAnnoSchema.NotMapped]
        public string ToFullName { get; set; }

    }
}