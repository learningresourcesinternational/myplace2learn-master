﻿extern alias DataAnnotations;
using System.ComponentModel;
using DataAnnotations.System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Collections.Generic;

namespace LRI.MyPlace2Learn.Models
{
    public class LearningPlanTask
    {
        public LearningPlanTask()
        {
            this.OnlineCourseScores = new HashSet<OnlineCourseScore>();
        }


        public int LearningPlanTaskId { get; set; }
        public int LearningResourceId { get; set; }
        public int UserId { get; set; }

        
        [DisplayFormat(DataFormatString="{0:dd/MM/yyyy}")]
        public System.DateTime? Started { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime? Finished { get; set; }
        
        
        public int AddedByUserId { get; set; }
        public System.DateTime AddedOn { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime ExpiryDate { get; set; } 

        public virtual LearningResource LearningResource { get; set; }
        public virtual User User { get; set; }

        [Display(Name="Time")]
        [DefaultValue(0)]
        public int? TotalTime { get; set; }

        public virtual ICollection<OnlineCourseScore> OnlineCourseScores { get; set; }
        
    }
}