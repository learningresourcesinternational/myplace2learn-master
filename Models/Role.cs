﻿extern alias DataAnnotations;
using System.ComponentModel;
using DataAnnotations.System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Collections.Generic;

namespace LRI.MyPlace2Learn.Models
{
    [Bind(Exclude = "RoleId")]
    public class Role
    {
        public Role()
        {
            this.Users = new HashSet<User>();
        }


        [ScaffoldColumn(false)]
        public int RoleId { get; set; }

        [DisplayName("Role")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public int CreatedByUserId { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? CreatedOn { get; set; }

        [ScaffoldColumn(false)]
        public int? UpdatedByUserId { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? UpdatedOn { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}