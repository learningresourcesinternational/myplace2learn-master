﻿extern alias DataAnnotations;
using System.ComponentModel;
using DataAnnotations.System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Collections.Generic;

namespace LRI.MyPlace2Learn.Models
{
    [Bind(Exclude = "UserId")]
    public class User
    {
        public User()
        {
            this.Roles = new HashSet<Role>();
        }

        [ScaffoldColumn(false)]
        public int UserId { get; set; }

        [DisplayName("First Name*")]
        [Required(ErrorMessage = "A first name is required")]
        [StringLength(100)]
        public string FirstName { get; set; }

        [DisplayName("Last Name*")]
        [Required(ErrorMessage = "A last name is required")]
        [StringLength(100)]
        public string LastName { get; set; }

        [DisplayName("Email*")]
        [Required(ErrorMessage = "Email addres is required")]
        public string Email { get; set; }
                
        [StringLength(50)]
        public string UserName { get; set; }
        
        [DisplayName("Password*")]
        [Required(ErrorMessage = "A Password is required")]
        [StringLength(50)]
        public string Password { get; set; }

        [ScaffoldColumn(false)]
        public int AddedByUserId { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime DateAdded { get; set; }

        [ScaffoldColumn(false)]
        [DefaultValue(true)]
        [DisplayName("Status*")]
        public bool IsActive { get; set; }

        [Required(ErrorMessage="A security question is required.")]
        [ScaffoldColumn(false)]
        [DisplayName("Please select a security question*")]
        public int DSSecurityQuestion { get; set; }

        [Required(ErrorMessage="A security answer is required.")]
        [DisplayName("Security Question Answer*")]
        public string SecurityQuestionAnswer { get; set; }

        
        [Required(ErrorMessage = "You must assign the user a role")]
        public virtual ICollection<Role> Roles { get; set; }

        
    }
}