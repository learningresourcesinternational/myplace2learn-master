﻿extern alias DataAnnotations;
using System;
using System.Collections.Generic;
using DataAnnotations.System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using LRI.MyPlace2Learn.Attributes;

namespace LRI.MyPlace2Learn.Models
{

    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
		[DataAnnotations.System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LogOnModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessage="First name required.")]
        [Display(Name = "First name*")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name required.")]
        [Display(Name = "Last name*")]
        public string LastName { get; set; }

        
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        [Required(ErrorMessage="Email address required.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address*")]
        public string Email { get; set; }

        [Required(ErrorMessage="Confirmation of email address required")]
        [Display(Name = "Confirm email address*")]
		[DataAnnotations.System.ComponentModel.DataAnnotations.Compare("Email", ErrorMessage = "The email and email confirmation addresses you have given to not match.")]
        public string ConfirmEmail { get; set; }

        [Required(ErrorMessage="Password required.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password*")]
        public string Password { get; set; }

        [Required(ErrorMessage="Confirmation of password required.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password*")]
		[DataAnnotations.System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Security Question required.")]
        [Display(Name = "Security Question*")]
        public int SecurityQuestion { get; set; }

        [Required(ErrorMessage = "Security question answer required.")]
        [Display(Name = "Answer*")]
        public string SecurityQuestionAnswer { get; set; }
        
        [BooleanRequired(ErrorMessage="You need to have read and agree to our Terms and Conditions.")]
        public bool TCAgreement { get; set; }

    }

    public class ForgottenPasswordModel
    {

        [Required]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password (Passwords need to contain at least 6 characters)")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
		[DataAnnotations.System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Security Question")]
        public int SecurityQuestion { get; set; }

        [Required]
        [Display(Name = "Answer")]
        public string SecurityQuestionAnswer { get; set; }
    
    }
}
