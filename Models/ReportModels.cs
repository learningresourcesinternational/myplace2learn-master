﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace LRI.MyPlace2Learn.Models
{
    public class RegistrationReportModel
    {
        public string RegistrationId { get; set; }
        public int Score { get; set; }
        public string CourseId { get; set; }
        public string Title { get; set; }
        public string LearnerId { get; set; }

    }
}