﻿extern alias DataAnnotations;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using DataAnnotations.System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace LRI.MyPlace2Learn.Models
{
    [Bind(Exclude = "NewsletterSubscriptionId")]
    public class NewsletterSubscription
    {
        public NewsletterSubscription()
        {
        }

        [ScaffoldColumn(false)]
        public int NewsletterSubscriptionId { get; set; }

        [DisplayName("First Name*")]
        [Required(ErrorMessage = "A first name is required")]
        [StringLength(100)]
        public string FirstName { get; set; }

        [DisplayName("Last Name*")]
        [Required(ErrorMessage = "A last name is required")]
        [StringLength(100)]
        public string LastName { get; set; }

        [DisplayName("Email*")]
        [Required(ErrorMessage = "An email is required")]
        [StringLength(150)]
        public string Email { get; set; }

        [DefaultValue(true)]
        public bool IsActive { get; set; }
        [DefaultValue(false)]
        public bool IsArchived { get; set; }
        
        public DateTime CreatedOn { get; set; }

    }
}