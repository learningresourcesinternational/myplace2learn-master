﻿extern alias DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel;
using DataAnnotations.System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;

namespace LRI.MyPlace2Learn.Models
{
    [Bind(Exclude = "OrderId")]
    public partial class Order
    {

        public Order()
        {
            this.OrderDetails = new HashSet<OrderDetail>();

            LearningContext learningDB = new LearningContext();

            string status = (from dsv in learningDB.DatasetValues
                             where dsv.DatasetValueId == this.DSStatus
                             select dsv.Value).SingleOrDefault();

        }
           

           [ScaffoldColumn(false)]
            public int OrderId { get; set; }

            [ScaffoldColumn(false)]
            public int UserId { get; set; }

            [ScaffoldColumn(false)]
            public System.DateTime OrderDate { get; set; }


            [ScaffoldColumn(false)]
            public decimal Total { get; set; }

            [ScaffoldColumn(false)]
            public decimal VATRate { get; set; }
            
            public string Identifier { get; set; }

            [ScaffoldColumn(false)]
            public string Username { get; set; }

            [ScaffoldColumn(false)]
            [DefaultValue((int) 3)]
            public int DSStatus { get; set; }

            [DataAnnotations.System.ComponentModel.DataAnnotations.Schema.NotMapped]
            public string Status { get; set; }
            [DataAnnotations.System.ComponentModel.DataAnnotations.Schema.NotMapped]
            public string GrossTotal { get { return (Total + (Total * VATRate / 100)).ToString("0.00"); } }

            
            
            public virtual ICollection<OrderDetail> OrderDetails { get; set; }


        #region old

        //[ScaffoldColumn(false)]
        //public int OrderId { get; set; }

        //[ScaffoldColumn(false)]
        //public System.DateTime OrderDate { get; set; }

       

        //[Required(ErrorMessage = "First Name is required")]
        //[DisplayName("First Name")]
        //[StringLength(160)]
        //public string FirstName { get; set; }

        //[Required(ErrorMessage = "Last Name is required")]
        //[DisplayName("Last Name")]
        //[StringLength(160)]
        //public string LastName { get; set; }

        //[Required(ErrorMessage = "Address is required")]
        //[StringLength(70)]
        //public string Address { get; set; }

        //[Required(ErrorMessage = "City is required")]
        //[StringLength(40)]
        //public string City { get; set; }

        //[Required(ErrorMessage = "State is required")]
        //[StringLength(40)]
        //public string State { get; set; }

        //[Required(ErrorMessage = "Postal Code is required")]
        //[DisplayName("Postal Code")]
        //[StringLength(10)]
        //public string PostalCode { get; set; }

        //[Required(ErrorMessage = "Country is required")]
        //[StringLength(40)]
        //public string Country { get; set; }

        //[Required(ErrorMessage = "Phone is required")]
        //[StringLength(24)]
        //public string Phone { get; set; }

        //[Required(ErrorMessage = "Email Address is required")]
        //[DisplayName("Email Address")]
        //[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
        //    ErrorMessage = "Email is is not valid.")]
        //[DataType(DataType.EmailAddress)]
        //public string Email { get; set; }

        //[ScaffoldColumn(false)]
        //public decimal Total { get; set; }

        //public List<OrderDetail> OrderDetails { get; set; }

        #endregion
    }
}
