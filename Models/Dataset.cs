﻿extern alias DataAnnotations;
using System.ComponentModel;
using DataAnnotations.System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Collections.Generic;

namespace LRI.MyPlace2Learn.Models
{
    [Bind(Exclude = "DatasetId")]
    public class Dataset
    {

        public Dataset()
        {
            this.DatasetValues = new HashSet<DatasetValue>();
        }

        [ScaffoldColumn(false)]
        public int DatasetId { get; set; }
        [Required]
        [DisplayName("Name")]
        public string Name { get; set; }

        [ScaffoldColumn(false)]
        public int CreatedByUserId { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime CreatedOn { get; set; }

        [ScaffoldColumn(false)]
        public int? UpdatedByUserId { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? UpdatedOn { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<DatasetValue> DatasetValues { get; set; }

    }
}