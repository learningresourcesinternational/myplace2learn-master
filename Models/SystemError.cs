﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Collections.Generic;

namespace LRI.MyPlace2Learn.Models
{
    [Bind(Exclude="SystemErrorId")]
    public class SystemError
    {
        public int SystemErrorId { get; set; }
        public string UrlErroring { get; set; }
        public string ErrorMessage { get; set; }
        public System.DateTime DateError { get; set; }
    }
}