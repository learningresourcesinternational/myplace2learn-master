﻿extern alias DataAnnotations;
using DataAnnotations.System.ComponentModel.DataAnnotations;

namespace LRI.MyPlace2Learn.Models
{
    public class Cart
    {
        [Key]
        public int RecordId { get; set; }
        public string CartId { get; set; }
        public int LearningResourceId { get; set; }
        public int Count { get; set; }
        public System.DateTime DateCreated { get; set; }

        public virtual LearningResource LearningResource { get; set; }
    }
}