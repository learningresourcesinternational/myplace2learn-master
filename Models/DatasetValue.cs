﻿extern alias DataAnnotations;
using System.ComponentModel;
using DataAnnotations.System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Collections.Generic;

namespace LRI.MyPlace2Learn.Models
{
    [Bind(Exclude = "DatasetValueId")]
    public class DatasetValue
    {

        [ScaffoldColumn(false)]
        public int DatasetValueId { get; set; }

        [Required]
        [DisplayName("Value")]
        public string Value { get; set; }

        [Required]
        [DisplayNameAttribute("Dataset")]
        public int DatasetId { get; set; }

        [ScaffoldColumn(false)]
        public int CreatedByUserId { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime CreatedOn { get; set; }

        [ScaffoldColumn(false)]
        public int? UpdatedByUserId { get; set; }

        [ScaffoldColumn(false)]
        public System.DateTime? UpdatedOn { get; set; }

        public bool IsActive { get; set; }

    }
}