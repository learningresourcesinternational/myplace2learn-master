﻿extern alias DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel;
using DataAnnotations.System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;

namespace LRI.MyPlace2Learn.Models
{
    [Bind(Exclude = "FeedbackId")]
    public partial class Feedback
    {

        public Feedback()
        {
            
        }


		[ScaffoldColumn(false)]
        public int FeedbackId { get; set; }

        public int Rating { get; set; }

        [DisplayName("Please add some comments to explain your rating here")]
        [StringLength(2000)]
        public string Comments { get; set; }

        public int UserId { get; set; }
        public System.DateTime DateCreated { get; set; }

        [DefaultValue(false)]
        public bool IsActive { get; set; }

        [DefaultValue(false)]
        public bool IsArchived { get; set; }

        [DefaultValue(false)]
        public bool Withdrawn { get; set; }

        [DataAnnotations.System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public User UserPosting
        {
            get { return (new LearningContext()).Users.Find(this.UserId); }
        }

        [DisplayName("Please enter a reason for withdrawing this feedback")]
        [StringLength(500)]
        public string WithdrawalReason { get; set; }
        public int? WithdrawnByUserId { get; set; }
        public System.DateTime? DateWithdrawn { get; set; }
        
        public virtual int LearningResourceId { get; set; }
       
    }
}
