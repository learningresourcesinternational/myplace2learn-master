﻿extern alias DataAnnotations;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel;
using DataAnnotations.System.ComponentModel.DataAnnotations;

namespace LRI.MyPlace2Learn.Models
{
    [Bind(Exclude = "OnlineCourseScoreId")]
    public class OnlineCourseScore
    {
        [ScaffoldColumn(false)]
        public int OnlineCourseScoreId { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Score")]
        public decimal Score { get; set; }

        [DefaultValue(false)]
        [DisplayName("Pass?")]
        public bool Pass { get; set; }

        [ScaffoldColumn(false)]
        public int LearningPlanTaskId { get; set; }

        [DisplayName("Achieved on ")]
        public System.DateTime AddedOn { get; set; }

   

    }
}