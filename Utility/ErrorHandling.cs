﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;

namespace LRI.MyPlace2Learn.Utility
{
    /// <summary>
    /// Utility class for handling errors (fairly basic at present)
    /// </summary>
    public static class ErrorHandling
    {

        #region public methods

        /// <summary>
        /// Records an error arising from paypal in the database and sends email to support (specified in config)
        /// </summary>
        /// <param name="values">Return nvp values from paypal</param>
        public static void RecordPayPalError(System.Collections.Specialized.NameValueCollection values)
        {
            Models.Email errorMail = getMailDetails();
            errorMail.Message = setUpErrorEmailMessage(errorMail.Message, values);
            Utility.Email.SendMail(errorMail);
            saveErrorToDB(errorMail.Message);
        }

        /// <summary>
        /// Records an error when learning plan task fails to save updated details
        /// </summary>
        /// <param name="learningPlanTask">Learning plan task details you are trying to update</param>
        /// <param name="err">System error message generated</param>
        public static void LearningPlanTaskError(LearningPlanTask learningPlanTask, SystemException err)
        {
            Models.Email errorMail = getMailDetails();
            errorMail.Message = setUpErrorMailMessage(errorMail.Message, learningPlanTask, err);
            Utility.Email.SendMail(errorMail);
            saveErrorToDB(errorMail.Message);
        }
                
        /// <summary>
        /// Records and emails an error when online course score fails to save
        /// </summary>
        /// <param name="onlineCourseScore">Online Course Score object we are attempting to save</param>
        /// <param name="err">System error message generated</param>
        public static void OnlineCourseScoreError(OnlineCourseScore onlineCourseScore, SystemException err)
        {
            Models.Email errorMail = getMailDetails();
            errorMail.Message = setUpErrorMailMessage(errorMail.Message, onlineCourseScore, err);
            Utility.Email.SendMail(errorMail);
            saveErrorToDB(errorMail.Message);
        }

        public static void ErrorMessage(string errormessage)
        {
            Models.Email errorMail = getMailDetails();
            errorMail.Message = errormessage;
            Utility.Email.SendMail(errorMail);
            saveErrorToDB(errorMail.Message);
        }

        /// <summary>
        /// Records and emails an error when feedback fails to save
        /// </summary>
        /// <param name="feedback">Feedback object we are attempting to save</param>
        /// <param name="err">System error message generated</param>
        public static void FeedbackError(Feedback feedback, SystemException err)
        {
            Models.Email errorMail = getMailDetails();
            errorMail.Message = setUpErrorMailMessage(errorMail.Message, feedback, err);
            Utility.Email.SendMail(errorMail);
            saveErrorToDB(errorMail.Message);
        }

        #endregion

        #region private methods

        private static void saveErrorToDB(string errormessage)
        {
            LearningContext learningDB = new LearningContext();
            learningDB.SystemErrors.Add(new SystemError
                                        {
                                            UrlErroring = HttpContext.Current.Request.ServerVariables["URL"],
                                            ErrorMessage = errormessage,
                                            DateError = System.DateTime.Now
                                        });
            learningDB.SaveChanges();
        }



        // gets email and sets it up with basic settings
        private static Models.Email getMailDetails()
        {
            Models.Email errorMail = Utility.Email.getEmail("ERRMSG");
            errorMail.Subject = errorMail.Subject.Replace("[site]", HttpContext.Current.Request.ServerVariables["HTTP_HOST"]);
            errorMail.To = System.Configuration.ConfigurationManager.AppSettings["SupportEmail"];
            return errorMail;
        }

        // sets up error message email
        private static string setUpErrorEmailMessage(string message, System.Collections.Specialized.NameValueCollection values)
        {
            string messageText = message.Replace("[site]", HttpContext.Current.Request.ServerVariables["HTTP_HOST"]);
            messageText = messageText.Replace("[url]", HttpContext.Current.Request.ServerVariables["URL"]);

            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();

            var items = values.AllKeys.SelectMany(values.GetValues, (k, v) => new { key = k, value = v });

            stringBuilder.Append("<ul>");
            foreach (var item in items)
            {
                stringBuilder.AppendFormat("<li>{0}: {1}</li>", item.key, item.value);
            }
            stringBuilder.Append("</ul>");

            messageText = messageText.Replace("[ErrorMessage]", stringBuilder.ToString());

            return messageText;
        }

        private static string setUpErrorMailMessage(string message, LearningPlanTask learningPlanTask, SystemException err)
        {
            string messageText = message.Replace("[site]", HttpContext.Current.Request.ServerVariables["HTTP_HOST"]);
            messageText = messageText.Replace("[url]", HttpContext.Current.Request.ServerVariables["URL"]);

            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            stringBuilder.AppendLine("<p>Details of the error are given below.</p>");
            stringBuilder.AppendFormat("<p>Exception message: {0}</p><p>Inner Exception: {1}</p><p>Source:{2}</p>", err.Message, err.InnerException, err.Source);
            stringBuilder.AppendLine("Learning Plan Task Details:</p>");
            stringBuilder.AppendFormat("Learning Plan Task Id: {0}<br/>UserId: {1}<br/>Date Started: {2}<br/>Date Finished: {3}<br/>TimeSpent: {4}", learningPlanTask.LearningPlanTaskId, learningPlanTask.UserId, learningPlanTask.Started, learningPlanTask.Finished, learningPlanTask.TotalTime);

            messageText = messageText.Replace("[ErrorMessage]", stringBuilder.ToString());
            return messageText;
        }

        private static string setUpErrorMailMessage(string message, OnlineCourseScore onlineCourseScore, SystemException err)
        {
            string messageText = message.Replace("[site]", HttpContext.Current.Request.ServerVariables["HTTP_HOST"]);
            messageText = messageText.Replace("[url]", HttpContext.Current.Request.ServerVariables["URL"]);

            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            stringBuilder.AppendLine("<p>Details of the error are given below.</p>");
            stringBuilder.AppendFormat("<p>Exception message: {0}</p><p>Inner Exception: {1}</p><p>Source:{2}</p>", err.Message, err.InnerException, err.Source);
            stringBuilder.AppendLine("<p>Online Course Score Details:</p>");
            stringBuilder.AppendFormat("Learning Plan Task ID: {0}<br/>Score: {1}<br/>Pass: {2}", onlineCourseScore.LearningPlanTaskId, onlineCourseScore.Score, onlineCourseScore.Pass);

            messageText = messageText.Replace("[ErrorMessage]", stringBuilder.ToString());
            return messageText;
        }

        private static string setUpErrorMailMessage(string message, Feedback feedback, SystemException err)
        {
            string messageText = message.Replace("[site]", HttpContext.Current.Request.ServerVariables["HTTP_HOST"]);
            messageText = messageText.Replace("[url]", HttpContext.Current.Request.ServerVariables["URL"]);

            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            stringBuilder.AppendLine("<p>Details of the error are given below.</p>");
            stringBuilder.AppendFormat("<p>Exception message: {0}<br/>Inner Exception: {1}<br/>Source:{2}", err.Message, err.InnerException, err.Source);
            stringBuilder.AppendLine("Feedback Details:</p>");
            stringBuilder.AppendFormat("UserId: {0}<br/>Rating: {1}<br/>Feedback: {2}", feedback.UserId.ToString(), feedback.Rating.ToString(), feedback.Comments);

            messageText = messageText.Replace("[ErrorMessage]", stringBuilder.ToString());
            return messageText;
        }

        #endregion
    }
}