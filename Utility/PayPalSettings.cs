﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace LRI.MyPlace2Learn.Utility
{

    /// <summary>
    /// Class containing paypal properties used in Paypal class
    /// </summary>
    public class PayPalSettings
    {
        /// <summary>
        /// Gets the paypal api url
        /// </summary>
        public static string ApiUrl
        { 
            get {
                return (Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandBox"])) ? "https://api-3t.sandbox.paypal.com/nvp" : "https://api-3t.paypal.com/nvp";
            }
        }

        /// <summary>
        /// Gets the cgi url
        /// </summary>
        public static string CgiUrl
        { 
            get {
                return (Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandBox"])) ? "https://www.sandbox.paypal.com/cgi-bin/webscr?" : "https://www.paypal.com/cgi-bin/webscr?";
            }
        }

        /// <summary>
        /// Gets the signature used in paypal account identification
        /// </summary>
        public static string Signature
        {
            get { return (Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandBox"])) ? ConfigurationManager.AppSettings["PaypalSig"] : ConfigurationManager.AppSettings["LIVEPaypalSig"]; }
        }

        /// <summary>
        /// Gets the User name for the account
        /// </summary>
        public static string UserName
        {
            get { return (Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandBox"])) ? ConfigurationManager.AppSettings["PaypalUsr"] : ConfigurationManager.AppSettings["LIVEPaypalUsr"]; }
        }

        /// <summary>
        /// Gets the password for the account
        /// </summary>
        public static string Password
        {
            get { return (Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandBox"])) ? ConfigurationManager.AppSettings["PaypalPwd"] : ConfigurationManager.AppSettings["LIVEPaypalPwd"]; }
        }

        /// <summary>
        /// Gets the return url used by paypal to redirect user back to site
        /// </summary>
        public static string ReturnUrl
        {
            get { return ConfigurationManager.AppSettings["ReturnURL"]; }
        }

        /// <summary>
        /// Gets the cancel url used by paypal to redirect user back to site if they cancel out of the paypal interface
        /// </summary>
        public static string CancelUrl
        {
            get { return ConfigurationManager.AppSettings["CancelURL"]; }
        }

        /// <summary>
        /// Gets the currency code to be used in the transaction: note that if this is not set Paypal defaults to $
        /// </summary>
        public static string CurrencyCode
        {
            get { return ConfigurationManager.AppSettings["CurrencyCode"]; }
        }

    }
}