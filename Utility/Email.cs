﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace LRI.MyPlace2Learn.Utility
{
    /// <summary>
    /// Static utility class for setting up and sending an email
    /// </summary>
    public static class Email
    {
        private static Models.LearningContext learningDB = new Models.LearningContext();

        #region Public methods
        /// <summary>
        /// Retrieves email based on mail reference, generates email and sends it out
        /// </summary>
        /// <param name="mailReference">reference of email to sent</param>
        /// <param name="recipientId">Id of user email is being sent to </param>
        /// <param name="order">Order that the email is being sent in reference to</param>
        public static void SendMail(string mailReference, int recipientId, Models.Order order)
        {
            Models.Email email = getEmail(mailReference, recipientId);
            email.Subject = setUpEmailSubject(email.Subject, "[orderNumber]", order.OrderId.ToString());
            email.Message = setUpEmailMessage(email.Message, order, email.ToFullName);
            SendMail(email);
        }

        /// <summary>
        /// Retrieves email based on reference, generates mail & sends it out
        /// </summary>
        /// <param name="mailReference">reference of email</param>
        /// <param name="feedback">feedback object email is about</param>
        /// <param name="courseName">name of course feedback is about</param>
        /// <param name="userName">user supplying feedback</param>
        public static void SendMail(string mailReference, Models.Feedback feedback, string courseName, string userName)
        {
            Models.Email email = getEmail(mailReference);
            email.Message = setUpEmailMessage(feedback, email.Message, courseName, userName);
            SendMail(email);
        }


        /// <summary>
        /// Retrieves email based on mail reference, generates it and sends it out
        /// </summary>
        /// <param name="mailReference">Reference of mail to be sent</param>
        /// <param name="user">User the email is about</param>
        public static void SendMail(string mailReference, Models.User user)
        {
            Models.Email email = getEmail(mailReference, user.UserId);
            email.To = user.Email;
            email.ToFullName = user.FirstName + " " + user.LastName;
            email.Message = setUpEmailMessage(email.Message, user, email.ToFullName);
            SendMail(email);
        }
        
        /// <summary>
        /// Sends an email using the information passed
        /// </summary>
        /// <param name="mailTo">Address the email is going to </param>
        /// <param name="mailFrom">Address sending the email</param>
        /// <param name="subject">Email subject</param>
        /// <param name="message">Email body</param>
        public static void SendMail(string mailTo, string mailFrom, string subject, string message)
        {
            Models.Email email = new Models.Email();
            email.To = mailTo;
            email.Sender = mailFrom;
            email.Subject = subject;
            email.Message = message;
            SendMail(email);
        }


        /// <summary>
        /// Sends supplied email
        /// </summary>
        /// <param name="email">Email to send</param>
        public static void SendMail(Models.Email email)
        {
            MailMessage mailMessage = new MailMessage();
            SmtpClient smtpServer = new SmtpClient();

            mailMessage.From = new MailAddress(email.Sender);

            mailMessage.To.Add(new MailAddress(email.To));


            mailMessage.Subject = email.Subject;
            mailMessage.Body = email.Message;
            mailMessage.IsBodyHtml = true;

            smtpServer.Send(mailMessage);
        }


        #endregion
        
        // gets email from database
        public static Models.Email getEmail(string emailRef, int recipientId = 0)
        {

            Models.Email email = learningDB.Emails.SingleOrDefault(er => er.Reference == emailRef);
            if (!email.HasSender)
            {
                email.Sender = ConfigurationManager.AppSettings["DefaultEmailSender"];
            }
            
            if (emailRef == "SUBSCRIBEENEWS")
            {
                email.To = ConfigurationManager.AppSettings["ContactUsEmail"];
            }
            else if (emailRef == "FEEDBACKNOTE")
            {
                email.To = ConfigurationManager.AppSettings["FeedbackNotificationEmail"];
            }
            else if (recipientId > 0 && !Convert.ToBoolean(ConfigurationManager.AppSettings["IsDEBUG"]))
            {
                getEmailRecipient(recipientId, ref email);
            }
            else
            {
                email.To = ConfigurationManager.AppSettings["TestSender"];
                email.ToFullName = ConfigurationManager.AppSettings["TestSenderName"];
            }

            return email;
        }
        
        #region private methods used to set up and send email
        // gets email address for a given user
        private static void getEmailRecipient(int userId, ref Models.Email email)
        {
            var userToEmail = (from u in learningDB.Users
                               where u.UserId == userId
                               select new
                               {
                                   emailaddress = u.Email,
                                   userfullname = u.FirstName + " " + u.LastName
                               }).FirstOrDefault();

            email.To = userToEmail.emailaddress;
            email.ToFullName = userToEmail.userfullname;

        }

        // sets up the email subject
        private static string setUpEmailSubject(string subject, string texttoswap, string newText)
        {
            return subject.Replace(texttoswap, newText);
        }


        // Sets up email message for an order
        private static string setUpEmailMessage(string message, Models.Order order, string recipientName)
        {
            string messageText = message.Replace("[UserName]", recipientName);

            if (order != null)
            {
                System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
                messageText = messageText.Replace("[orderdate]", order.OrderDate.ToString());
                
                stringBuilder.Append("<tr><td>Course</td><td>Cost</td></tr>");

                List<Models.OrderDetail> orderDetails = learningDB.OrderDetails.Where(o => o.OrderId == order.OrderId).ToList();

                decimal orderTotal = 0;

                foreach (Models.OrderDetail orderDetail in orderDetails)
                {
                    decimal costWithVAT = UtilityFunction.VATCalculation(orderDetail.LearningResource.Cost) + orderDetail.LearningResource.Cost;
                    orderTotal += costWithVAT;
                    stringBuilder.Append("<tr>");
                    stringBuilder.AppendFormat("<td>{0}</td>", orderDetail.LearningResource.Title);
                    stringBuilder.AppendFormat("<td>{0}</td>", costWithVAT.ToString("0.00"));
                    stringBuilder.Append("</tr>");
                }

                stringBuilder.AppendFormat("<tr><td>Order Total</td><td>{0}</td></tr>", orderTotal.ToString("0.00"));

                messageText = messageText.Replace("[CourseList]", stringBuilder.ToString());
            }

            return messageText;
        }

        private static string setUpEmailMessage(Models.Feedback feedback, string message, string course, string userName)
        {
            string messageText = message.Replace("[Course]", course);
            messageText = messageText.Replace("[Feedback]", feedback.Comments);
            messageText = messageText.Replace("[UserName]", userName);
            return messageText;
        }

        // Sets up email message for registration email
        private static string setUpEmailMessage(string message, Models.User user, string recipientName)
        {
            string messageText = message.Replace("[UserName]", recipientName);
            return messageText;
        }
        
        #endregion
    }
}