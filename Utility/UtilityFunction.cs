﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;

namespace LRI.MyPlace2Learn.Utility
{
    /// <summary>
    /// Static class containing utility functions used throughout the application
    /// </summary>
    public static class UtilityFunction
    {
        /// <summary>
        /// Calculates the VAT supplied by the config file
        /// </summary>
        /// <param name="amount">Amount to pay VAT on</param>
        /// <returns>Calculated VAT on the given amount</returns>
        public static decimal VATCalculation(decimal amount)
        {
            if (amount > 0)
            {
                decimal vat = Convert.ToDecimal(ConfigurationManager.AppSettings["VAT"]);
                return Math.Round(amount * (vat / 100), 2);
            }
            return 0;
        }


        private static System.TimeSpan FormatTimeFromSeconds(int seconds)
        {
            return System.TimeSpan.FromSeconds(seconds);
        }

        /// <summary>
        /// Displays time in x hours y minutes format
        /// </summary>
        /// <param name="seconds">Seconds to convert to display time</param>
        /// <returns></returns>
        public static string DisplayTimeFromSeconds(int seconds)
        {
            System.TimeSpan timeSpan = FormatTimeFromSeconds(seconds);
            return string.Format("{0:D1} hours {1:D2} minutes", timeSpan.Hours, timeSpan.Minutes);
        }

    }


}