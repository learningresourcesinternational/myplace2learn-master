﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using PdfSharp.Drawing;
using PdfSharp.Pdf;


namespace LRI.MyPlace2Learn.Utility
{
    public static class Pdf
    {

        public static PdfDocument GeneratePDF(LRI.MyPlace2Learn.Models.LearningPlanTask lpt)
        {
            // Create new document...
            PdfDocument doc = new PdfDocument();
            doc.Info.Title = "Course Certificate";

            // Create an empty page...
            PdfPage page = doc.AddPage();

            // Create an XGraphics object for drawing...
            XGraphics gfx = XGraphics.FromPdfPage(page);

            // Create a font
            XFont font = new XFont("Verdana", 38, XFontStyle.Bold);

            // Retrieve correct background colour...
            string imageFile = "../../Content/" + System.Configuration.ConfigurationManager.AppSettings["Client"] + "/Images/certBackground250dpi.jpg";

            string imageServerPath = HttpContext.Current.Server.MapPath(imageFile);

            XImage logoImage = XImage.FromFile(imageServerPath);
            int logoHeight = logoImage.PixelHeight;
            int logoWidth = logoImage.PixelWidth;

            page.Height = logoHeight;
            page.Width = logoWidth;

            // Apply background image...
            gfx.DrawImage(logoImage, 0, 0);

            // Draw the user's name...
            gfx.DrawString(lpt.User.FirstName + " " + lpt.User.LastName, font, XBrushes.Black, new XRect(0, -200, page.Width, page.Height), XStringFormats.Center);

            // Draw the Course Title...
            gfx.DrawString(lpt.LearningResource.Title, font, XBrushes.Black, new XRect(0, 50, page.Width, page.Height), XStringFormats.Center);

            // Display score if we have one...
            if (lpt.OnlineCourseScores.Count > 0)
            {
                gfx.DrawString(lpt.OnlineCourseScores.Max(s => s.Score).ToString() + "%", font, XBrushes.Black, new XRect(0, 100, page.Width, page.Height), XStringFormats.Center);
            }

            return doc;

        }

    }
}