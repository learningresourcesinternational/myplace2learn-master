﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Net;

namespace LRI.MyPlace2Learn.Utility
{
    /// <summary>
    /// Static utility class to set up and send Paypal payment communications
    /// </summary>
    public static class Paypal
    {

        #region public methods
        /// <summary>
        /// Sets up and generates redirect url to send user to Paypal
        /// </summary>
        /// <param name="orderId">Id of the order being paid for</param>
        /// <param name="orderTotal">Total of the order being paid for</param>
        /// <returns>Redirect url</returns>

        public static string PayPalExpressCheckout(int orderId, decimal orderTotal)
        {
            NameValueCollection payPalValues = new NameValueCollection();

            setMethodCredentials("SetExpressCheckout", ref payPalValues);
            payPalValues["RETURNURL"] = PayPalSettings.ReturnUrl + orderId.ToString();
            payPalValues["CANCELURL"] = PayPalSettings.CancelUrl + orderId.ToString();
            payPalValues["PAYMENTREQUEST_0_AMT"] = orderTotal.ToString("0.00");
            payPalValues["BUTTONSOURCE"] = "PP-ECWizard";
            payPalValues["NOSHIPPING"] = "1";
            payPalValues["PAYMENTREQUEST_0_PAYMENTACTION"] = "Sale";
            payPalValues["PAYMENTREQUEST_0_CURRENCYCODE"] = PayPalSettings.CurrencyCode;
            payPalValues["L_PAYMENTREQUEST_0_NAME0"] = "E-Learning Order " + orderId.ToString();
            payPalValues["L_PAYMENTREQUEST_0_AMT0"] = orderTotal.ToString("0.00");
            
            NameValueCollection payPalResponseValues = submitToPayPal(payPalValues);

            string ack = payPalResponseValues["ACK"].ToLower();

            if (ack == "success" || ack == "successwithwarning")
            {
                return String.Format("{0}cmd=_express-checkout&token={1}", PayPalSettings.CgiUrl, payPalResponseValues["TOKEN"]);
            }
            else
            {
                payPalResponseValues["ERRORTYPE"] = "PayPal error at SetExpressCheckout";
                Utility.ErrorHandling.RecordPayPalError(payPalResponseValues);             
                throw new Exception(payPalResponseValues["L_LONGMESSAGE0"]); 
            }
        }

        /// <summary>
        /// Retrieves paypal transaction details using given token and finalises payment
        /// </summary>
        /// <param name="token">Token supplied by paypal to identify the transaction</param>
        /// <param name="orderTotal">Order Total being charged</param>
        /// <returns>True if no errors reported by paypal, false otherwise</returns>
        public static bool PaymentOK(string token, decimal orderTotal)
        {
            NameValueCollection payPalCheckoutDetails = getExpressCheckoutDetails(token);
            string ack = payPalCheckoutDetails["ACK"].ToLower();
            string payer = payPalCheckoutDetails["PAYERID"];
            string tokenreturned = payPalCheckoutDetails["TOKEN"];

            if (ack == "success" || ack == "successwithwarning")
            {
                return expressPaymentOk(token, payer, orderTotal);
            }
            else
            {
                payPalCheckoutDetails["ERRORTYPE"] = "PayPal error at GetExpressCheckoutDetails";
                Utility.ErrorHandling.RecordPayPalError(payPalCheckoutDetails);   
            }
            return false;
        }


        #endregion

        #region private methods

        // Generate the query string from supplied NVP values, sends information to PayPal and returns NVP for paypal response
        // Used by any method needing to send and retreive from PayPal
        private static NameValueCollection submitToPayPal(NameValueCollection payPalValues)
        {

            string querystringdata = String.Join("&", payPalValues.Cast<string>()
                                                      .Select(k => String.Format("{0}={1}", k, HttpUtility.UrlEncode(payPalValues[k]))));

            string api = PayPalSettings.ApiUrl;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(PayPalSettings.ApiUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = querystringdata.Length;

            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(querystringdata);
            }
            using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
            {   
                return HttpUtility.ParseQueryString(reader.ReadToEnd());
            }
        }

        // Call function for DoExpressCheckoutPayment and returns true/false dependent on response from Paypal
        private static bool expressPaymentOk(string token, string payer, decimal orderTotal)
        {
            NameValueCollection payPalCheckoutDetails = doExpressCheckoutPayment(token, payer, orderTotal);
            string ack = payPalCheckoutDetails["ACK"].ToLower();
            if (ack == "success" || ack == "successwithwarning")
            {
                return true;
            }
            else
            {
                payPalCheckoutDetails["ERRORTYPE"] = "PayPal error at DoExpressCheckoutPayment";
                Utility.ErrorHandling.RecordPayPalError(payPalCheckoutDetails);
            }
            return false;
        }
        
        // Retrieves express checkout details using the token supplied in the initial setexpresscheckout call
        private static NameValueCollection getExpressCheckoutDetails(string token)
        {
            NameValueCollection payPalValues = new NameValueCollection();
            setMethodCredentials("GetExpressCheckoutDetails", ref payPalValues);
            payPalValues["TOKEN"] = token;
            return submitToPayPal(payPalValues);
        }

        // Calls DoExpressCheckoutPayment to finalise payment
        private static NameValueCollection doExpressCheckoutPayment(string token, string payer, decimal amount)
        {
            NameValueCollection payPalValues = new NameValueCollection();
            setMethodCredentials("DoExpressCheckoutPayment", ref payPalValues);
            payPalValues["PAYMENTREQUEST_0_PAYMENTACTION"] = "Sale";
            payPalValues["PAYMENTREQUEST_0_CURRENCYCODE"] = PayPalSettings.CurrencyCode;
            payPalValues["PAYMENTREQUEST_0_AMT"] = amount.ToString("0.00");
            payPalValues["PAYERID"] = payer;
            payPalValues["TOKEN"] = token;
            return submitToPayPal(payPalValues);
        }

        // Adds User credentials and method to NVP for api call
        private static void setMethodCredentials(string method, ref NameValueCollection payPalValues)
        {
            payPalValues["METHOD"] = method;
            payPalValues["USER"] = PayPalSettings.UserName;
            payPalValues["PWD"] = PayPalSettings.Password;
            payPalValues["SIGNATURE"] = PayPalSettings.Signature;
            payPalValues["VERSION"] = "89.0";
        }
        #endregion
    }   
}