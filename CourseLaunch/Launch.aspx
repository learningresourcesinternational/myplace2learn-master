﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Launch.aspx.cs" Inherits="Lri.Emerald.SmallBusiness.Pages.CourseLaunch.Launch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server">	
	<script src="../Scripts/jquery-1.9.0.min.js"></script>
	<script src="Scripts/Cmi.js"></script>
	<script src="Scripts/AiccRequests.js"></script>
	<script type="text/javascript">

		function CallService() {
				$.ajax({
					type: "POST",
					url: "<%= ResolveClientUrl("/CourseLaunch/Handler/TrackingService.ashx") %>",
						dataType: "json",
						data: { "TYP": 0, "TaskID": $("#<%: hdnTaskID.ClientID %>").val(), "resourceID": $("#<%: hdnResourceID.ClientID %>").val(), "userID": $("#<%: hdnUserID.ClientID %>").val(), "itemId": $("#<%: hdnItemID.ClientID %>").val(), "launchtype": $("#<%: hdnLaunchType.ClientID %>").val(), "standard": $("#<%: hdnStandard.ClientID %>").val() },
					async: false,
					success: OnSuccess,
					error: OnError
					});
			}

			function OnSuccess(data, status) {
				var dt = new Date();
				document.getElementById('<%= hdnStartDate.ClientID %>').value = dt.getTime().toString();
				if (data.scormTrackingInfo != null) {
					populateCmiCallBack(data.scormTrackingInfo);
				}
				else {
					//alert('AICC Course');
					launchAiccCourse(data.aiccTrackingInfo, "<%= ResolveClientUrl(HttpContext.Current.Request.Url.Host + ":" + Request.Url.Port + "/Pages/CourseLaunch/Handler/TrackingService.ashx") %>");
				}
				
			}

			function OnError(request, status, error) {
				alert("--> " + request.statusText);
			}

			function refreshHere() {				
				$.ajax({
					url: "../../../Handlers/Learning/LearningResourceSCORM.ashx",
					type: "POST",
					async: false,
					cache: false,
					data: {
						ACT: "KeepAlive",
						LPTID: "123"
					},
					success: function (status) {
						//alert(status+' success in refresher in launch.aspx (Aviva)');
					},
					error: function () {
						//alert("failed in refresher in launch.aspx (Aviva)");
					}
				});
				setTimeout('refreshHere()', 60000); // 60000 milliseconds , use one minute for Live running
			}
			function KeepAlive() {
				refreshHere();
			}


		</script>
	<link href="<%= "../../../App_Themes/" + _theme + "/custom.css"  %>" type="text/css" rel="stylesheet" />
    <title></title>
</head>
<body onload="CallService();KeepAlive();"">
	<form id="Launch" runat="server">
			<asp:HiddenField ID="hdnTaskID" runat="server" ClientIDMode="Static" />
			<asp:HiddenField ID="hdnUserID" runat="server" ClientIDMode="Static" />
			<asp:HiddenField ID="hdnResourceID" runat="server" ClientIDMode="Static" />
			<asp:HiddenField ID="hdnItemID" runat="server" ClientIDMode="Static" />
			<asp:HiddenField ID="hdnStartDate" runat="server" ClientIDMode="Static" />
			<asp:HiddenField ID="hdnLaunchType" runat="server" ClientIDMode="Static" />
			<asp:HiddenField ID="hdnStandard" runat="server" ClientIDMode="Static" />
		</form>
	
		<div class="wrapper">
			<div class="contentMain container_16">
				<form id="frmPopupLauncher">
					<div id="MessageAreaWrapper" style="width: 60%; margin-left: auto; margin-right: auto;">						
						<div id="Message" align="center">
							<h3>Your course has been launched in a new window.</h3>
						</div>
						<div id="PossiblePopupBlockerMessage" align="center">
							We launched your course in a new window but if you do not see it, a popup blocker may be preventing it from opening. Please disable popup blockers for this site.
							To relaunch please click here <input type="button" onclick="CallService(); KeepAlive()" value="Launch Course" />
						</div>
						<br>
						<h3 align="center"><a href="<%=ResolveClientUrl("../LearningPlan/")%>">Return to LMS</a></h3>
					</div>
				</form>
			</div>
		</div>

		<!-- Show course history details here -->


</body>
</html>
