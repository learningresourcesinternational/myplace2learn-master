﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lri.TrackingEngine.DAL.MyPlace2Learn;
using Lri.TrackingEngine.Domain;

namespace Lri.Emerald.SmallBusiness.Pages.CourseLaunch
{
	public partial class Launch : System.Web.UI.Page
	{
		private TrackerBL _trakerBL;
		protected string _theme = "lri";

		public Launch()
		{
			_trakerBL = new TrackerBL(new TrackerRepo());
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
			{
				Response.Redirect("~/Pages/Learning/MyLearning.aspx");
			}

			// Check that this page has been bookmarked or accessed directly...
			if (Request.QueryString.Count > 0)
			{
				setTheme();
				checkParams();
			}
			else
			{
				Response.Redirect("~/Pages/Learning/MyLearning.aspx");
			}          
		}

		private void checkParams()
		{
			if (Request.QueryString["TaskID"] != null)
			{
				int taskID = Convert.ToInt32(Request.QueryString["TaskID"]);

				if (taskID > 0)
				{
					getLaunchSettings(taskID);
				}
			}
		}

		private void setTheme()
		{
			if (Session["CurrentTheme"] != null)
			{
				_theme = Session["CurrentTheme"].ToString();
			}
		}

		private void getLaunchSettings(int taskID)
		{			
			var launchInfo = _trakerBL.GetLaunchInfo(taskID);

			if (launchInfo != null)
			{
				hdnTaskID.Value = taskID.ToString();
				hdnResourceID.Value = launchInfo.LearningResourceID.ToString();
				hdnUserID.Value = launchInfo.UserID.ToString();
				hdnItemID.Value = launchInfo.ItemID.ToString();
				hdnLaunchType.Value = launchInfo.LaunchType.ToString();
				hdnStandard.Value = launchInfo.Standard.ToString();
			}
		}
		
	}
}