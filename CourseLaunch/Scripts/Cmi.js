/*
*   Authors: Mat White, Tommy Ahearne
*   Description: 
*/


var cmiList = [];
var errorLog = [];
var redirect = "../LearningPlan/";

// 0: Read and Write. 1: Read-Only 2: Write-Only

var cmiProperties = [
['cmi.core.student_id', 1],
['cmi.core.student_name', 1],
['cmi.core.lesson_location', 0],
['cmi.core.credit', 1],
['cmi.core.lesson_status', 0],
['cmi.core.entry', 1],
['cmi.core.score.raw', 0],
['cmi.core.score.min', 0],
['cmi.core.score.max', 0],
['cmi.core.total_time', 1],
['cmi.core.lesson_mode', 1],
['cmi.core.exit', 2],
['cmi.core.session_time', 2],
['cmi.suspend_data', 0],
['cmi.launch_data', 1],
['cmi.comments', 0],
['cmi.comments_from_lms', 1],
['cmi.student_data.mastery_score', 1],
['cmi.student_data.max_time_allowed', 1],
['cmi.student_data.time_limit_action', 1],
['cmi.student_preference.audio', 0],
['cmi.student_preference.language', 0],
['cmi.student_preference.speed', 0],
['cmi.student_preference.text', 0],

['cmi.objectives._count', 1],
['cmi.objectives.{n}.id', 0],
['cmi.objectives.{n}.score.raw', 0],
['cmi.objectives.{n}.score.max', 0],
['cmi.objectives.{n}.score.min', 0],
['cmi.objectives.{n}.status', 0],

['cmi.interactions._count', 1],
['cmi.interactions.{n}.id', 2],
['cmi.interactions.{n}.objectives._count', 1],
['cmi.interactions.{n}.objectives.{x}.id', 2],
['cmi.interactions.{n}.time', 2],
['cmi.interactions.{n}.type', 2],
['cmi.interactions.{n}.correct_responses._count', 1],
['cmi.interactions.{n}.correct_responses.{x}.pattern', 2],
['cmi.interactions.{n}.weighting', 2],
['cmi.interactions.{n}.student_response', 2],
['cmi.interactions.{n}.result', 2],
['cmi.interactions.{n}.latency', 2]];

function populateCmiCallBack(trackingData) {
	try {
		cmiList = [];
        cmiList.push(trackingData.UserID); // 0 UserID
        cmiList.push(trackingData.StudentName); // 1 StudentName
        cmiList.push(trackingData.LessonLocation); // 2 LessonLocation
        cmiList.push(trackingData.Credit); // 3 Credit
        cmiList.push(trackingData.Status); // 4 LessonStatus
        cmiList.push(''); // 5 Entry
        cmiList.push(trackingData.ScoreRaw); // 6 ScoreRaw
        cmiList.push(trackingData.ScoreMin); // 7 ScoreMin
        cmiList.push(trackingData.ScoreMax); // 8 ScoreMax
        cmiList.push(trackingData.TotalTime); // 9 TotalTime
        cmiList.push(''); // 10 LessonMode
        cmiList.push(trackingData.Exit); // 11 Exit
        cmiList.push(''); // 12 SessionTime
        cmiList.push(trackingData.SuspendData); // 13 SuspendData
        cmiList.push(trackingData.LaunchData); // 14 LaunchData
        cmiList.push(''); // 15 Comments 
        cmiList.push(''); // 16 Comments From LMS
        cmiList.push(trackingData.MasteryScore); // 17 MasteryScore
        cmiList.push(''); // 18 Max Time Allowed
        cmiList.push(''); // 19 Time Limit Action
        cmiList.push(''); // 20 Audio
        cmiList.push(''); // 21 Language
        cmiList.push(''); // 22 Speed
        cmiList.push(''); // 23 Text

        // The following 18 empty values account for Objectives and interactions of which DREAM does not support, 
        // but must be able to offer a minumum of an empty string to the scorm course
        cmiList.push('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

        launchScormCourse(trackingData.Location);
        //appInsights.startTrackEvent(trackingData.Location);
        
    } catch (e) {
        logError(e.toString());
        reportError();
        return false;
    }
}

function launchScormCourse(scoPath) {
    try {
        // First Remove any existing instance of ApiManager from the Documents Scripts
        var scripts = document.getElementsByTagName('script');
        for (var i = 0; i < scripts.length; i++) {
            // This check determines whether the script is a document or script object block
            if (typeof (scripts[i].getAttribute('src')) == 'string') {
                if (scripts[i].getAttribute('src').indexOf('Scorm.ApiManager.js') != -1)
                    scripts[i].parentNode.removeChild(scripts[i])
            }
        }

        // Attach new instance of ApiManager to window
        var apiManager = document.createElement('script');
        apiManager.setAttribute("type", "text/javascript");
        apiManager.setAttribute("src", "Scripts/Scorm.ApiManager.js");
        document.getElementsByTagName("head")[0].appendChild(apiManager);

        // Launch course in new window
        var win = window.open(scoPath, 'scormwindow', 'menubar=no,toolbar=no,resizable=yes,scrollbars=1', '');
        if (!win)
            alert('A popup was blocked. Please make an exception for this site in your popup blocker and try again');
    } catch (e) {
        logError(e.toString());
        reportError();
        return false;
    }
}

function finaliseCmi() {

	try {
        $.ajax({
            type: "POST",
            url: "Handler/TrackingService.ashx",
            data: { "TYP": 1, "taskID": $("#hdnTaskID").val(), "resourceID": $("#hdnResourceID").val(), "itemId": $("#hdnItemID").val(), "userId": cmiList[0], "lessonLocation": cmiList[2], "lessonStatus": cmiList[4], "credit": cmiList[3], "exit": cmiList[11], "sessionTime": cmiList[12], "totalTime": cmiList[9], "suspendData": cmiList[13], "scoreRaw": cmiList[6], "scoreMax": cmiList[8], "scoreMin": cmiList[7], "startDate": $("#hdnStartDate").val(), "standard": $("#hdnStandard").val() },
            dataType: "json",
            success: OnPersistSuccess,
            error: OnPersistError
        });
    }
    catch (e) {
        logError(e.message);
        reportError();
    }
    //finally
    //{
    //    cmiList = [];
    //}
}

function OnPersistSuccess(data, status) {
   // appInsights.stopTrackEvent(cmiList[17]);
    if (cmiList[4] == "completed") {
        //alert('Course Completed')
        //window.location.reload();
    }
    else {
        //alert('course abandoned')
        //window.location.reload();
    }
	// Do we actually want to redirect?
    window.location = redirect;
}

function OnPersistError(request, status, error) {
    alert('error')
}

function logError(errorMessage) {
    errorLog.push(errorMessage);
}

function reportError() {
    var errorHtml = '';
    for (var i = 0; i < errorLog.length; i++)
        errorHtml += '<p class=\'error_html\'>' + errorLog[i] + '<\p>';
    alert(errorHtml)
    // TODO: ADAM, YOU NEED TO DISPLAY THIS ERROR SOMEHOW
}


