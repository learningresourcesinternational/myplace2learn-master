/*
*   Authors: Tommy Ahearne, Mat White
*   Description: 
*/

var errorCodes = new Array();
errorCodes[0] = "No Error";
errorCodes[101] = "General exception";
errorCodes[201] = "Invalid argument error";
errorCodes[202] = "Element cannot have children";
errorCodes[203] = "Element is not an array - cannot have count";
errorCodes[301] = "Not initialised";
errorCodes[401] = "Not implemented error";
errorCodes[402] = "Invalid set value, element is a keyword";
errorCodes[403] = "Element is read only";
errorCodes[404] = "Element is write only";
errorCodes[405] = "Incorrect Data type";

var lastError = 0;

var errorDiagnostics = new Array();
errorDiagnostics[0] = "No Error occurred during the last action.";
errorDiagnostics[101] = "A general exception has occurred.";
errorDiagnostics[201] = "The Data Model Element supplied, does not exist";
errorDiagnostics[202] = "The Data Model Element supplied, does not support the '_children' keyword";
errorDiagnostics[203] = "The Data Model Element supplied, is not an array and does not support the '_count' keyword.";
errorDiagnostics[301] = "The API has not been initialised. Initialise the API by calling 'LMSInitialize()'.";
errorDiagnostics[401] = "The Data Model Element supplied, is either outside the SCORM Run-Time Environment or is not supported by the LMS.";
errorDiagnostics[402] = "The Data Model Element supplied, is a keyword and cannot have its value reset.";
errorDiagnostics[403] = "The property supplied is Read-Only.";
errorDiagnostics[404] = "The property supplied is Write-Only.";
errorDiagnostics[405] = "The value supplied is an incorrect type for the specified Data Model Element.";

var sessionState;

function Create2001API() {
    sessionState = "Not Initialized";
    this.LMSInitialize = Initialize_2001_Sco;
    this.LMSFinish = Finish_2001_Sco;
    this.LMSGetValue = GetValue_2001_Sco;
    this.LMSSetValue = SetValue_2001_Sco;
    this.LMSCommit = Commit_2001_Sco;
    this.LMSGetLastError = GetLastError_2001_Sco;
    this.LMSGetErrorString = GetErrorString_2001_Sco;
    this.LMSGetDiagnostic = GetDiagnostic_2001_Sco;
}

function Initialize_2001_Sco(param) {
    if (param == "") {
        switch (sessionState) {
            case "Not Initialized":
                sessionState = "Initialized";
                return "true";
                break;
            default:
                lastError = 101;
                return "false";
                break;
        }
    } else {
        lastError = 201;
        return "false";
    }
}

function Finish_2001_Sco(param) {
    if (param == "") {
        switch (sessionState) {
            case "Initialized":
                //send CMI back to client
                if (lastError == 0) {
                    sessionState = "Finished";
                    API = null;
                    finaliseCmi();
                    return "true";
                } else {
                    return "false";
                }
                break;
            default:
                lastError = 101;
                return "false";
                break;
        }
    } else {
        lastError = 201;
        return "false";
    }

}

function GetValue_2001_Sco(propertyName) {
    switch (sessionState) {
        case "Initialized":
            propertyName = propertyName.toLowerCase();
            if (propertyName.indexOf(".") != -1) {
                // RETURN GET VALUE FROM CMI
                var index = GetPropertyIndex(propertyName);
                if (index != -1) {
                    if (cmiProperties[index][1] == 2) {
                        lastError = 404;
                        return '';
                    }
                    else {
                        return cmiList[index].toString();
                    }
                }
                else {
                    lastError = 401;
                    return '';
                }
            } else {
                lastError = 405;
                return '';
            }
            break;
        default:
            lastError = 301;
            return '';
            break;
    }
}

function SetValue_2001_Sco(key, value) {
    switch (sessionState) {
        case "Initialized":
            var index = GetPropertyIndex(key);
            if (index != -1) {
                if (cmiProperties[index][1] == 1) {
                    lastError = 403;
                    return 'false';
                }
                else {
                    cmiList[index] = value;
                    return 'true';
                }
            }
            else {
                lastError = 401;
                return 'false';
            }
            break;
        default:
            lastError = 301;
            return "false";
            break;
    }
}

function Commit_2001_Sco(param) {
	/**
	 * COMMIT MUST EXIST FOR A VALID API, HOWEVER, WE DO NOT IMPLEMENT
	 * THIS METHOD AS WE ARE NOT USING AD-HOC DATA PERSISTENCE
	 **/ 
    if (param == "") {
        switch (sessionState) {
            case "Initialized":
                lastError = 0;
                return "true";
            default:
                lastError = 301;
                return "false";
                break;
        }
    } else {
        lastError = 201;
        return "false";
    }
}

function GetLastError_2001_Sco() {
    return lastError + '';
}

function GetErrorString_2001_Sco(errorCode) {
    var errorString;

    errorCode = parseInt(errorCode);
    if (errorCode.toString() != "NaN") {
        errorString = errorCodes[errorCode];
        if (errorString == null) {
            errorString = "";
        }
    } else {
        errorString = "";
    }
    return errorString;
}

function GetDiagnostic_2001_Sco(param_In) {
    var diagnosticString

    if (param_In == "") {
        diagnosticString = errorDiagnostics[lastError];
    } else {
        var errorCode = parseInt(param_In);
        if (errorCode.toString() != "NaN") {
            diagnosticString = errorDiagnostics[errorCode];
            if (diagnosticString == null) {
                diagnosticString = "";
            }
        } else {
            diagnosticString = "";
        }
    }
    return diagnosticString;
}

function GetPropertyIndex(propertyName) {
    for (var i = 0; i < cmiProperties.length; i++) {
        if (cmiProperties[i][0].indexOf(propertyName) != -1) {
            return i;
        }
    }
    return -1;
}

var API = new Create2001API();