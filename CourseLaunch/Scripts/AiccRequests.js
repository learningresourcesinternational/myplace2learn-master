﻿function launchAiccCourse(trackingData, aiccURL) {
	try {
		
		launchURL = trackingData.Location + "?&aicc_sid=" + trackingData.LearningResourceID + "&aicc_url=" + aiccURL + "?AICCQSData=2|" + trackingData.LearningPlanTaskID + "|" + trackingData.UserID + "|" + trackingData.ItemID + "|" + trackingData.LearningResourceID;

		// Launch course in new window
		var win = window.open(launchURL, 'aiccwindow', 'menubar=no,toolbar=no,resizable=yes,scrollbars=1', '');
		if (!win)
			alert('A popup was blocked. Please make an exception for this site in your popup blocker and try again');
	} catch (e) {
		logError(e.toString());
		reportError();
		return false;
	}
}
