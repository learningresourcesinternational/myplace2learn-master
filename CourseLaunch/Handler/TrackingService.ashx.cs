﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lri.TrackingEngine.Domain;
using Lri.TrackingEngine.Domain.DTOs;
using Lri.TrackingEngine.DAL.MyPlace2Learn;

namespace ScoreBoard.Web.Services
{
	
	public enum ServiceRequestType
	{
		GetItem,
		InsertItem,
		AiccRequest
	}

	public class TrackingService : IHttpHandler
	{
		private TrackerBL _trackerBL;
		
		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			string qsType = string.Empty;
			string taskID = string.Empty;
			string resourceID = string.Empty;
			string userID = string.Empty;
			string itemID = string.Empty;

			// What was the request...
			if (context.Request.QueryString["AICCQSData"] != null)
			{

				// For some reason (I cannot see at the moment) I cannot include the required info as QS params on the AICC_URL param, so I am cheating.
				//AICCQSData=TYP|LPTID|USERID|ItemID
				string urlData = context.Request.QueryString["AICCQSData"];
				string[] aiccQSData = urlData.Split('|');

				qsType = aiccQSData[0];
				taskID = aiccQSData[1];				
				userID = aiccQSData[2];
				itemID = aiccQSData[3];
				resourceID = aiccQSData[4];
									
			}
			else
			{
				qsType = context.Request.Form["TYP"];
				taskID = context.Request.Form["taskID"];
				resourceID = context.Request.Form["resourceID"];
				userID = context.Request.Form["userID"];
				itemID = context.Request.Form["itemID"];

			}

			// AICC Items..
			string command = context.Request.Form["command"];
			string aiccData = context.Request.Form["aicc_data"];


			// The item values below could be set as global rather than  passing into method but when we use the WebAPI we would pass directly into method,
			// so it should make migration a little easier...
			string launchType = context.Request.Form["launchType"];
			string standard = context.Request.Form["standard"];
			
			// Tracking vars...
			string lessonLoaction = context.Request.Form["lessonlocation"];
			string lessonStatus = context.Request.Form["lessonstatus"];
			string credit = context.Request.Form["credit"];
			string exit = context.Request.Form["exit"];
			string sessionTime = context.Request.Form["sessionTime"];
			string totalTime = context.Request.Form["totaltime"];
			string suspendData = context.Request.Form["suspenddata"];
			string scoreRaw = context.Request.Form["scoreraw"];
			string scoreMax = context.Request.Form["scoremax"];
			string scoreMin = context.Request.Form["scoremin"];
			string startDate = context.Request.Form["startdate"];

			_trackerBL = new TrackerBL(new TrackerRepo(), standard);
			
			// OK, so what are we doing...
			ServiceRequestType type = (ServiceRequestType)Enum.Parse(typeof(ServiceRequestType), qsType, true);

			switch (type)
			{
				case ServiceRequestType.GetItem:
					context.Response.Write(JsonConvert.SerializeObject(GetItemUserTracking(resourceID, userID, itemID, launchType, standard)));
					break;
				case ServiceRequestType.InsertItem: // Change name to Process Scorm?
					context.Response.Write(JsonConvert.SerializeObject(InsertItemUserTrackingService(taskID, resourceID, itemID, userID, lessonLoaction, lessonStatus, credit, exit, sessionTime, totalTime, suspendData, scoreRaw, scoreMax, scoreMin, startDate)));
					break;
				case ServiceRequestType.AiccRequest:
					context.Response.Write(ProcessAiccRequest(command, aiccData, taskID, resourceID, itemID, userID, launchType));
					break;
				default:

					break;
			}
		}

		private ItemUserTracking GetItemUserTracking(string resourceID, string userID, string itemID, string launchType, string standard)
		{
			int resID = Convert.ToInt32(resourceID);
			int usrID = Convert.ToInt32(userID);
			int itmID = Convert.ToInt32(itemID);
			int launchTyp = Convert.ToInt32(launchType);

			return _trackerBL.GetUserTrackingInfo(resID, itmID, usrID, launchTyp, standard);

		}

		private int InsertItemUserTrackingService(string taskID, string resourceID, string itemId, string userID, string lessonLocation, string lessonStatus,
        string credit, string exit, string sessionTime, string totalTime, string suspendData, string scoreRaw, string scoreMax, string scoreMin, 
        string startDate)
		{
			int lptID = Convert.ToInt32(taskID);
			var result = _trackerBL.CreateTrackingItem(lptID, resourceID, userID, itemId, lessonLocation, lessonStatus, credit, exit, sessionTime, totalTime, suspendData, scoreRaw, scoreMax, scoreMin, startDate);

			return result.scormTrackingInfo.LearningPlanTaskID;
		}

		private string ProcessAiccRequest(string command, string aiccData, string taskID, string resourceID, string itemID, string userID, string launchType)
		{
			int resID = Convert.ToInt32(resourceID);
			int usrID = Convert.ToInt32(userID);
			int itmID = Convert.ToInt32(itemID);
			int launchTyp = Convert.ToInt32(launchType);
			int lptID = Convert.ToInt32(taskID);

			switch (command.ToUpper())
			{
				case "GETPARAM":
					var itemUserTracking = _trackerBL.GetUserTrackingInfo(resID, itmID, usrID, launchTyp, "AICC");
					return itemUserTracking.aiccTrackingInfo.AiccData;
				case "PUTPARAM":
					var result = _trackerBL.CreateTrackingItem(lptID, resourceID, userID, itemID, aiccData);
					return result.aiccTrackingInfo.AiccData;
				default:
					break;
			}

			return "";
		}


		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}