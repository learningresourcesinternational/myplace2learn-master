﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;
using LRI.MyPlace2Learn.Attributes;
using PagedList;
//using PagedList.Mvc;

namespace LRI.MyPlace2Learn.Controllers
{
    [Authorize]
    public class LearningResourceController : Controller
    {
        private LearningContext db = new LearningContext();

        //
        // GET: /LearningResource/
        [LayoutSetter("_LayoutNoLeftMenu")]
        public ActionResult Index(int? page)
        {
            IQueryable<LearningResource> learningResources = db.LearningResources.Where(l=>l.IsActive == true).OrderBy(l=>l.Title);
            IPagedList<LearningResource> resourcePage = learningResources.ToPagedList(page ?? 1, 10);
            return View(resourcePage);
        }

        //
        // GET: /LearningResource/Details/5
        public ActionResult Details(int id = 0)
        {
            if (id > 0)
            {
                LearningResource learningresource = db.LearningResources.Find(id);
                                                    
                if (learningresource == null)
                {
                    return HttpNotFound();
                }
                return View(learningresource);
            }

            return View();
        }

        //
        // GET: /LearningResource/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /LearningResource/Create

        [HttpPost]
        public ActionResult Create(LearningResource learningresource)
        {
            if (ModelState.IsValid)
            {
                db.LearningResources.Add(learningresource);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(learningresource);
        }

        //
        // GET: /LearningResource/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (id == 0)
            { 
                
            }

            LearningResource learningresource = db.LearningResources.Find(id);
            if (learningresource == null)
            {
                return HttpNotFound();
            }
            return View(learningresource);
        }

        //
        // POST: /LearningResource/Edit/5

        [HttpPost]
        public ActionResult Edit(LearningResource learningresource)
        {
            if (ModelState.IsValid)
            {
                db.Entry(learningresource).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(learningresource);
        }

        //
        // GET: /LearningResource/Delete/5

        public ActionResult Delete(int id = 0)
        {
            LearningResource learningresource = db.LearningResources.Find(id);
            if (learningresource == null)
            {
                return HttpNotFound();
            }
            return View(learningresource);
        }

        //
        // POST: /LearningResource/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            LearningResource learningresource = db.LearningResources.Find(id);
            db.LearningResources.Remove(learningresource);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}