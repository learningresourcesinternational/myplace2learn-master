﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;
using LRI.MyPlace2Learn.Attributes;
using PagedList;
//using PagedList.Mvc;

namespace LRI.MyPlace2Learn.Controllers
{
    [Authorize]
    [LayoutSetter("_LayoutNoLeftMenu")]
    public class LearningCategoryController : Controller
    {
        private LearningContext db = new LearningContext();

        //
        // GET: /LearningCategory/

        public ActionResult Index(int? page, int recordStatus = 1)
        {
            RecordStatusDisplay displayStatus = (RecordStatusDisplay)recordStatus;
            IQueryable<LearningCategory> query = db.LearningCategories;

            switch (displayStatus)
            { 
                case RecordStatusDisplay.Active:
                    query = query.Where(lc => lc.IsActive == true);
                    break;
                case RecordStatusDisplay.Inactive:
                    query = query.Where(lc => lc.IsActive == false);
                    break;
                case RecordStatusDisplay.All:
                    break;
            }

            return View(query.OrderBy(lc=>lc.Name).ToPagedList(page ?? 1, 10));
        }

        //
        // POST: /LearningCategory/
        [HttpPost]
        public ActionResult Index()
        {
            string status = Request.Form["status"];

            IQueryable<LearningCategory> learningCategories = db.LearningCategories
                                                              .OrderBy(c => c.Name);

            if (!string.IsNullOrEmpty(Request.Form["searchTerm"]))
            {
                learningCategories = learningCategories
                                     .Where(c => c.Name == Request.Form["searchTerm"]);
            }

            IPagedList<LearningCategory> filteredCategories = learningCategories.ToPagedList(1, 10);
            return View(filteredCategories);
        }
                
        //
        // GET: /LearningCategory/Details/5
        public ActionResult Details(int id = 0)
        {
            LearningCategory learningcategory = db.LearningCategories.Find(id);
            if (learningcategory == null)
            {
                return HttpNotFound();
            }
            return View(learningcategory);
        }
        
        //
        // POST: /LearningCategory/Create

        [HttpPost]
        public ActionResult Create(LearningCategory learningcategory)
        {
            if (ModelState.IsValid)
            {
                db.LearningCategories.Add(learningcategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(learningcategory);
        }

        //
        // GET: /LearningCategory/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LearningCategory learningcategory = db.LearningCategories.Find(id);
            if (learningcategory == null)
            {
                return HttpNotFound();
            }
            return View(learningcategory);
        }

        //
        // POST: /LearningCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(LearningCategory learningcategory)
        {
            if (ModelState.IsValid)
            {

                learningcategory.IsActive = (Request.Form["IsActive"] == "1");
                learningcategory.IsArchived = (Request.Form["IsActive"] == "0");
                learningcategory.LastUpdatedBy = Convert.ToInt32(Session["UserId"]);
                learningcategory.LastUpdatedOn = System.DateTime.Now;

                db.Entry(learningcategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(learningcategory);
        }

        //
        // GET: /LearningCategory/Archive/5
        public ActionResult Archive(int id, bool isArchive = true)
        {
            archive(db.LearningCategories.Find(id), isArchive);
            return RedirectToAction("Index");
        }

        private void archive(LearningCategory category, bool isArchive)
        { 
            category.IsActive = (isArchive) ? false : true;
            category.IsArchived = (isArchive) ? true : false;
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}