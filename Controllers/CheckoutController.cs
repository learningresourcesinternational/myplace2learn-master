﻿using System;
using System.Linq;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using LRI.MyPlace2Learn.Attributes;

namespace LRI.MyPlace2Learn.Controllers
{
    [Authorize]
    public class CheckoutController : Controller
    {
      
        LearningContext learningDB = new LearningContext();
      
        //
        // GET: /Checkout/AddressAndPayment?id=5
        [LayoutSetter("_LayoutNoLeftMenu")]
        public ActionResult AddressAndPayment(int? id)
        {
            // if id is not equal to null so user has cancelled from PayPal (after order has been created)
            // so remove order from database
            if (id != null)
            {
                learningDB.Orders.Remove(learningDB.Orders.Find(id));
                learningDB.SaveChanges();
            }


            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Set up our ViewModel
            var viewModel = new ViewModels.ShoppingCartViewModel
            {
                CartItems = cart.GetCartItems(),
                CartTotal = cart.GetTotal()
            };

            viewModel.VATTotal = Utility.UtilityFunction.VATCalculation(viewModel.CartTotal);
            viewModel.OrderTotal = viewModel.CartTotal + viewModel.VATTotal;


            // Return the view
            return View(viewModel);
        }

        //
        // POST: /Checkout/AddressAndPayment
        [HttpPost]
        public ActionResult AddressAndPayment(FormCollection values)
        {
            
            Order order = new Order();
            TryUpdateModel(order);
            
            try
            {
                order.UserId = Convert.ToInt32(System.Web.HttpContext.Current.Session["UserId"]);
                order.OrderDate = DateTime.Now;
                order.DSStatus = 3;
                order.VATRate = Convert.ToDecimal(ConfigurationManager.AppSettings["VAT"]);

                learningDB.Orders.Add(order);
                learningDB.SaveChanges();

                var cart = ShoppingCart.GetCart(this.HttpContext);
                cart.CreateOrder(order);
                
                learningDB.SaveChanges();

                decimal total = order.Total + Utility.UtilityFunction.VATCalculation(order.Total);

                return RedirectToAction("PostToPayPal", new {orderid = order.OrderId, orderTotal = total});
            }
            catch (System.Exception err)
            {
                return View();  
            }

        }
        
        private void sendConfirmationEmail(Order order)
        {   
            Utility.Email.SendMail("ORDERCONF", Convert.ToInt32(HttpContext.Session["UserId"]), order);
        }
        
        /// <summary>
        /// Redirects to paypal to process the payment
        /// </summary>
        /// <param name="orderId">Id of the order being paid for</param>
        /// <param name="orderTotal">Total of order</param>
        /// <returns>Redirect result</returns>
        public RedirectResult PostToPayPal(int orderId, decimal orderTotal)
        {   
            string redirect = Utility.Paypal.PayPalExpressCheckout(orderId, orderTotal);
            return new RedirectResult(redirect);
        }


        //
        // GET: /Checkout/CompletePayment/id
        public ActionResult CompletePayment(int id, string token)
        {
            int currentUser = Convert.ToInt32(HttpContext.Session["UserId"]);
            Order order = learningDB.Orders.SingleOrDefault(o => o.OrderId == id && o.UserId == currentUser);

            bool paymentOK = Utility.Paypal.PaymentOK(token, Utility.UtilityFunction.VATCalculation(order.Total) + order.Total);

            if (paymentOK)
            {
                order.DSStatus = 6;
                learningDB.SaveChanges();
                sendConfirmationEmail(order);

                // set up learning plan tasks
                foreach (OrderDetail orderDetail in order.OrderDetails)
                {
                    createLearningPlanTask(orderDetail.LearningResourceId, currentUser);
                }

                // Empty the shopping cart
                var cart = ShoppingCart.GetCart(this.HttpContext);
                
                cart.EmptyCart();

                return View(id);

            }

            return View("Error");
        }



        private void createLearningPlanTask(int learningResourceId, int currentUser)
        {  

            LearningPlanTask learningPlanTask = new LearningPlanTask
            {
                LearningResourceId = learningResourceId,
                Started = null,
                UserId = currentUser,
                AddedByUserId = currentUser,
                AddedOn = DateTime.Now,
                Finished = null,
                ExpiryDate = DateTime.Now.AddMonths(3),
                TotalTime = 0
            };

            learningDB.LearningPlanTasks.Add(learningPlanTask);
            learningDB.SaveChanges();
        }

    }
}
