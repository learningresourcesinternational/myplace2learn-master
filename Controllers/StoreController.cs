﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;
using PagedList;
//using PagedList.Mvc;
using LRI.MyPlace2Learn.ViewModels;

namespace LRI.MyPlace2Learn.Controllers
{
    public class StoreController : Controller
    {
        LearningContext learningDB = new LearningContext();
        private readonly DAL.Repositories.IStoreRepository repository = new DAL.Repositories.StoreRepository();

        //
        // GET: /Store/
        public ActionResult Index()
        {
            ////var genres = storeDB.Genres.ToList();
            ////return View(genres);

            return View(learningDB.LearningResources.Where(c => c.IsActive == true && c.IsFeatured == true).ToList());
        }

        //
        // GET: /Store/Browse?catid=5
        [HttpGet]
        public ActionResult Browse(int? catid, int? page, string searchterm)
        {
           IQueryable<LearningResource> learningResources = (string.IsNullOrEmpty(searchterm)) ? this.filter((int) catid) : this.filter(searchterm);
           IPagedList<LearningResource> storePage = learningResources.ToPagedList(page ?? 1, 5);
		   
           return View(storePage);
        }

        //
        // POST: /Store/Browse/searchTerm
        [HttpPost]
        public ActionResult Browse()
        {
            string searchTerm = Request.Form["searchTerm"];
            // only call to this will be when form is submitted for a search so page start will always be 1
            IPagedList<LearningResource> storePage = filter(searchTerm).ToPagedList(1, 5); 
            return View(storePage);
        }
        
        //
        // GET: /Store/Details/5
        public ActionResult Details(int id) 
        {
            LearningResource resource = learningDB.LearningResources.Find(id);
            return View(resource);
        }
        
        private IQueryable<LearningResource> filter(int catid)
        {
            return learningDB.LearningResources
                   .OrderBy(lr => lr.Title)
                   .Where(lr => lr.LearningCategories.Any(lc => lc.LearningCategoryID == catid) && lr.IsActive == true && (lr.ParentID == null || lr.ParentID == 0));
        }

        private IQueryable<LearningResource> filter(string searchTerm)
        { 
            return learningDB.LearningResources
                   .OrderBy(lr=>lr.Title)
                   .Where(lr => lr.Title.Contains(searchTerm) && lr.IsActive == true && (lr.ParentID == null || lr.ParentID == 0));
        }


        //
        // GET: /Store/CategoryMenu
        [ChildActionOnly]
        public ActionResult CategoryMenu()
        {
            return PartialView(repository.GetMenu());
        }

    }
}