﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;
//using RusticiSoftware.HostedEngine.Client;
using LRI.MyPlace2Learn.Attributes;
using System.IO;
using PdfSharp;
using LRI.MyPlace2Learn.ViewModels;
using PagedList;
//using PagedList.Mvc;

namespace LRI.MyPlace2Learn.Controllers
{ 
    
    public class LearningPlanController : Controller
    {
        private LearningContext learningDb = new LearningContext();
        private readonly DAL.Repositories.LearningPlanTaskRepository repository = new DAL.Repositories.LearningPlanTaskRepository();
        
        //
        // GET: /LearningPlan/
        [LayoutSetter("_LayoutNoLeftMenu")] 
        [Authorize]       
        public ViewResult Index(int? id, int? page)
        {
            int currentUser = Convert.ToInt32(HttpContext.Session["UserId"]);
            if (id != null)
            {   
               // processRegistrationResult((int) id);
            }           
            return View(repository.ListLearningPlanTasks(currentUser).ToPagedList(page ?? 1, 8));
        }


		//private RedirectResult processRegistrationResult(int learningPlanTaskId)
		//{
		//	BusinessLayer.LearningPlanTaskProcessor processor = new BusinessLayer.LearningPlanTaskProcessor();
		//	if (!processor.ProcessRegistrationResult(learningPlanTaskId))
		//	{
		//		TempData["ERRORMESSAGE"] = "Our apologies an error appears to have occurred when we tried to save your progress through the course. An email has been sent to out to our support administrator.";
		//	}
		//	return Redirect("LearningPlan");
		//}
      
        //
        // AJAX: /LearningPlan/GetLaunchUrl/5
		//[HttpPost]
		//public string GetLaunchUrl(int lptid)
		//{
		//	BusinessLayer.LearningPlanTaskProcessor processor = new BusinessLayer.LearningPlanTaskProcessor();
		//	string  urlReferrer = Request.UrlReferrer.ToString() ;
		//	return processor.GetLaunchUrl(lptid, HttpContext.Session["UserId"].ToString(), HttpContext.Session["FirstName"].ToString(), HttpContext.Session["LastName"].ToString(), urlReferrer);          
		//}

        //
        // AJAX: /LearningPlan/GetScores/5
        //[HttpPost]
        public JsonResult GetScores(int id)
        {
            DAL.Repositories.OnlineCourseScoreRepository repository = new DAL.Repositories.OnlineCourseScoreRepository();

            return Json(from s in repository.ListOnlineCourseScores(id)
                        select new ScoresViewModel
                        {
                            DateAcheived = s.AddedOn.ToShortDateString() + " at " + s.AddedOn.ToShortTimeString(),
                            Pass = (s.Pass) ? "Pass" : "Fail",
                            Score = s.Score
                        });
        }


        //
        // AJAX: /LearningPlan/GetLearningPlanTaskDetails/5  
        [HttpPost]
        public JsonResult GetLearningPlanTaskDetails(int id)
        {
            if (HttpContext.Session != null && HttpContext.User.Identity.IsAuthenticated)
            {

                LearningPlanTask lpt = learningDb.LearningPlanTasks.Find(id);

                LearningPlanTaskViewModel lptViewModel = new LearningPlanTaskViewModel
                                                            {
                                                                ResourceTitle = lpt.LearningResource.Title,
                                                                LearningTime = Utility.UtilityFunction.DisplayTimeFromSeconds((int)lpt.TotalTime),
                                                                HasExpired = (System.DateTime.Now > lpt.ExpiryDate) ? true : false,
                                                                Started = "Not Started",
                                                                Completed = "n/a",
                                                                IsScored = lpt.LearningResource.Scored,
                                                                HasTest = false
                                                            };


                if (lpt.Started != null && lpt.TotalTime > 0)
                {
                    lptViewModel.Started = lpt.Started.Value.ToShortDateString();
                    lptViewModel.Completed = (lpt.Finished == null) ? "In progress" : lpt.Finished.Value.ToShortDateString();
                }


                if (!lpt.LearningResource.Scored)
                {
                    lptViewModel.HasTest = learningDb.LearningResources.Any(lr => lr.Scored == true && lr.ParentID == lpt.LearningResourceId);
                }

                return Json(lptViewModel);
            }

            return Json("TIMEDOUT", "application/json");
        }

        // AJAX: /LearningPlan/ResetRegistration/5
        [HttpPost]
        public string ResetRegistration(int id)
        {

            if(HttpContext.Session != null && HttpContext.User.IsInRole("LRIAdmin"))
            {
                try
                {
                    LearningPlanTask lpt = learningDb.LearningPlanTasks.Find(id);
                    lpt.Finished = null;
                    lpt.Started = null;
                    lpt.TotalTime = 0;

                    List<OnlineCourseScore> scores = learningDb.OnlineCourseScores.Where(o => o.LearningPlanTaskId == id).ToList();

                    foreach (OnlineCourseScore score in scores)
                    {
                        learningDb.OnlineCourseScores.Remove(score);
                    }
                
                    learningDb.SaveChanges();

                    return "SUCCESS";
                }
                catch (SystemException err)
                {
                    return "FAIL";
                }
            }

            return "FAIL";


        }


        // GET: /LearningPlan/Certificate/5
        [HttpGet]
        public ActionResult Certificate(int id)
        {
            PdfSharp.Pdf.PdfDocument pdf = Utility.Pdf.GeneratePDF(learningDb.LearningPlanTasks.Find(id));

            using (MemoryStream stream = new MemoryStream())
            {
                pdf.Save(stream, false);
                return File(stream.ToArray(), "application/pdf");
            }

        }

        protected override void Dispose(bool disposing)
        {
            learningDb.Dispose();           
            base.Dispose(disposing);
        }

        // GET: /LearningPlanTask/SCORMRegistrationList
        public ActionResult SCORMRegistrationList()
        {
            return View(repository.GetRegistrationsList());
        }


    }
}