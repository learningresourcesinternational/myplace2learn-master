﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using LRI.MyPlace2Learn.Models;
using LRI.MyPlace2Learn.Attributes;

namespace LRI.MyPlace2Learn.Controllers
{
    public class AccountController : Controller
    {
        private LRIMembershipProvider membershipProvider = (LRIMembershipProvider)Membership.Provider;
        private LearningContext learningDB = new LearningContext();

        private void MigrateShoppingCart(string UserName)
        {
            // Associate shopping cart items with logged-in user
            var cart = ShoppingCart.GetCart(this.HttpContext);    

            cart.MigrateCart(UserName);
            Session[ShoppingCart.CartSessionKey] = UserName;
        }

        //
        // GET: /Account/LogOn
        [LayoutSetter("_LayoutNoLeftMenu")]
        public ActionResult LogOn()
        {
            return View();
        }

        //
        // POST: /Account/LogOn
        [HttpPost]
        [LayoutSetter("_LayoutNoLeftMenu")]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (membershipProvider.ValidateUser(model.UserName, model.Password))
                {
                    MigrateShoppingCart(model.UserName); 
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            this.HttpContext.Session.Clear();
            this.HttpContext.Session.Abandon();

            System.Web.HttpCookie tempCookie = new System.Web.HttpCookie(FormsAuthentication.FormsCookieName);
            tempCookie.Expires = DateTime.Now.AddYears(-1);
            tempCookie.Value = "";

            System.Web.HttpCookie cookie2 = new System.Web.HttpCookie("ASP.NET_SessionId", "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);

            this.HttpContext.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            Request.Cookies.Clear();
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register
        [LayoutSetter("_LayoutNoLeftMenu")]
        public ActionResult Register()
        { 
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {

            LRIMembershipProvider provider = (LRIMembershipProvider)Membership.Provider;

            if (ModelState.IsValid)
            {

                User user = new User();
                TryUpdateModel(user);

                if (checkUserDoesNotAlreadyExist(user.Email))
                {
                    ModelState.AddModelError("", "We already have an account for this email. If you can not remember your password please use the reset password link to create a new one.");
                }
                else
                {
                    user.UserName = model.Email;
                    user.AddedByUserId = 0;
                    user.DSSecurityQuestion = model.SecurityQuestion;
                    user.DateAdded = System.DateTime.Now;
                    user.IsActive = true;
                    user.Roles.Add(learningDB.Roles.SingleOrDefault(rl => rl.RoleId == 2));
                    learningDB.Users.Add(user);
                    learningDB.SaveChanges();
                    
                    FormsAuthentication.SetAuthCookie(user.UserName, false /* createPersistentCookie */);
                    this.HttpContext.Session["UserId"] = user.UserId;
                    this.HttpContext.Session["FirstName"] = user.FirstName;
                    this.HttpContext.Session["LastName"] = user.LastName;

                    sendRegistrationConfirmationEmail(user);

                    return RedirectToAction("Index", "Home");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }



        //
        // GET: /Account/ChangePassword
        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }


        //
        // GET: /Account/ForgottenPassword
        public ActionResult ForgottenPassword()
        {
            return View();
        }


        //
        // POST: /Account/ForgottenPassword
        [HttpPost]
        public ActionResult ForgottenPassword(ForgottenPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                User user = getUser(model);

                if (user == null)
                {
                    ModelState.AddModelError("", "Password reset failed: we could not identify you from your email address and security question.");

                }
                else if (!user.IsActive)
                {
                    ModelState.AddModelError("", "Your account appears to be inactive. Please contact us to reactivate it.");
                }
                else
                {
                    user.Password = model.Password;
                    learningDB.SaveChanges();
                    return RedirectToAction("ChangePasswordSuccess");
                }

            }

            return View(model);
        }

        #region private methods

        private User getUser(ForgottenPasswordModel model)
        {
            return (User)learningDB.Users.SingleOrDefault(u => u.DSSecurityQuestion == model.SecurityQuestion && u.SecurityQuestionAnswer == model.SecurityQuestionAnswer && u.UserName == model.Email);
        }

        private void sendRegistrationConfirmationEmail(User user)
        {
            Utility.Email.SendMail("REGN", user);
        }

        private bool checkUserDoesNotAlreadyExist(string email)
        {
            return (bool)learningDB.Users.Any(u => u.Email == email);
        }
        
        #endregion


        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
