﻿#region namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using LRI.MyPlace2Learn.Models;
#endregion

#region Project References
using SessionEntity = System.Web.HttpContext;
#endregion



namespace LRI.MyPlace2Learn.Controllers
{
    public class LRIMembershipProvider : System.Web.Security.MembershipProvider
    {
        #region private members
        private string applicationName = ConfigurationManager.AppSettings["applicationname"];
        private int maxInvalidPasswordAttempts;
        private bool enablePasswordReset;
        private int minRequiredPasswordLength;
        private bool requiresPasswordChange = false;
        private int passwordTimeout;
        private DateTime passwordLastChangedDate;
        #endregion

        #region overridden public properties
        public override string ApplicationName
        {
            get { return applicationName; }
            set { applicationName = value; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return maxInvalidPasswordAttempts; }
        }

        public override bool EnablePasswordReset
        {
            get { return enablePasswordReset; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return minRequiredPasswordLength; }
        }

        #endregion

        #region public properties

        /// <summary>
        /// Gets or sets any login error messages
        /// </summary>
        public string LoginErrorMessage { get; set; }

        /// <summary>
        /// Gets the time period that a password can be used before it expires
        /// </summary>
        public int PasswordTimeout
        {
            get { return passwordTimeout; }
        }

        /// <summary>
        /// Boolean set by the Validation process to indicate user needs to change their password
        /// </summary>
        public bool RequiresPasswordChange
        {
            get { return requiresPasswordChange; }
            set { requiresPasswordChange = value; }
        }

        #endregion

        #region methods
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "LRIMembershipProvider";
            }

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "ELearning System");
            }

            base.Initialize(name, config);

            
            passwordTimeout = Convert.ToInt32(config["passwordTimeout"]);

        }


        /// <summary>
        /// Checks that user is a valid user and creates relevant session variables
        /// </summary>
        /// <param name="username">Username of the user attempting to login</param>
        /// <param name="password">Password of the user attempting to login</param>
        /// <returns>Boolean if user is valid</returns>
        public override bool ValidateUser(string username, string password)
        {
            LoginErrorMessage = string.Empty;

            LearningContext learningDB = new LearningContext();

            try
            {
                User currentUser = learningDB.Users.Where(u=> u.UserName == username && u.Password == password).SingleOrDefault();

                if (currentUser != null) 
                {
                    SessionEntity.Current.Session["UserId"] = currentUser.UserId;
                    SessionEntity.Current.Session["FirstName"] = currentUser.FirstName;
                    SessionEntity.Current.Session["LastName"] = currentUser.LastName;
                    
                   
                    System.Web.Security.FormsAuthentication.SetAuthCookie(username, false);

                    return true;
                }
                else
                {
                    LoginErrorMessage = "Invalid Username or Password.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoginErrorMessage = "Problem with login. Error: " + ex.Message.ToString();
                return false;
            }

        }

        public bool ForgotPasswordUserDetails(string username)
        {




            return false;
        }

        /// <summary>
        /// Private funtion to check if password has timed out
        /// </summary>
        /// <returns>True if password has timed out, false otherwise</returns>
        private bool passwordTimedOut()
        {
            return (Convert.ToInt32(DateTime.Now.Subtract(passwordLastChangedDate).TotalDays) > passwordTimeout) ? true : false;
        }


        #endregion

        #region properties and methods currently not implemented

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out System.Web.Security.MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }



        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override System.Web.Security.MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }


        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }



        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override System.Web.Security.MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(System.Web.Security.MembershipUser user)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
