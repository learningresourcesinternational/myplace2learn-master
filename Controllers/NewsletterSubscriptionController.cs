﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;

namespace LRI.MyPlace2Learn.Controllers
{
    public class NewsletterSubscriptionController : Controller
    {
        private LearningContext db = new LearningContext();

        //
        // GET: /NewsletterSubscription/Subscribe

        public ActionResult Subscribe()
        {
            return View();
        }

        //
        // POST: /NewsletterSubscription/Subscribe

        [HttpPost]
        public ActionResult Subscribe(NewsletterSubscription newslettersubscription)
        {
            if (ModelState.IsValid)
            {
                newslettersubscription.CreatedOn = System.DateTime.Now;
                newslettersubscription.IsActive = true;
                db.NewsletterSubscriptions.Add(newslettersubscription);
                db.SaveChanges();

                Email email = Utility.Email.getEmail("SUBSCRIBEENEWS");
                email.Message = email.Message.Replace("[SubScriber]", newslettersubscription.FirstName + " " + newslettersubscription.LastName);
                email.Message = email.Message.Replace("[Email]", newslettersubscription.Email);
                Utility.Email.SendMail(email);

                return Redirect("SubscribeComplete");
            }

            return View("Subscribe");
        }


        public ActionResult SubscribeComplete()
        {
            return View();
        }

    }
}