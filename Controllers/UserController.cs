﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;
using LRI.MyPlace2Learn.Attributes;
using PagedList;
//using PagedList.Mvc;

namespace LRI.MyPlace2Learn.Controllers
{
    [Authorize]
    [LayoutSetter("_LayoutNoLeftMenu")]
    public class UserController : Controller
    {
        private LearningContext db = new LearningContext();

        //
        // GET: /User/
        public ActionResult Index(int? page, int? active)
        {
            bool isActive = (active == 0)? false : true;
            return View(db.Users.Where(u=>u.IsActive == isActive).OrderBy(u=>u.LastName).ThenBy(u=>u.FirstName).ToPagedList(page?? 1, 1));
        }
               
        //
        // GET: /User/Edit/5        
        public ActionResult Edit(int id = 0)
        {
            IList<Role> roles = db.Roles.OrderBy(r=>r.Name).ToList();
            ViewBag.Roles = (IEnumerable<SelectListItem>)roles.Select(r => new SelectListItem { Text = r.Name, Value = r.RoleId.ToString() });  

            if (id > 0)
            {
                User user = db.Users.Include("Roles").FirstOrDefault(u=>u.UserId == id);
                if (user == null)
                {
                    return HttpNotFound();
                }             
                return View(user);
            }
            else
            {              
                return View();
            }
        }

        //
        // POST: /User/Edit/5
        [HttpPost]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                user.UserId = Convert.ToInt32(Request.Form["UserId"]);
                user.IsActive = (Request.Form["IsActive"] == "1");
                user.UserName = user.Email;
                user.DateAdded = System.DateTime.Now;
                user.AddedByUserId = Convert.ToInt32(Session["UserId"]);

                if (user.UserId > 0)
                {
                    var userRoles = new HashSet<int>(user.Roles.Select(r => r.Users.First().UserId));

                    db.Entry(user).State = EntityState.Modified;
                
                }
                else
                {
                    db.Entry(user).State = EntityState.Added;
                }
                
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

            
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}