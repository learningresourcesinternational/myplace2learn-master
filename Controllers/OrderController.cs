﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;
using LRI.MyPlace2Learn.Attributes;
using PagedList;
//using PagedList.Mvc;

namespace LRI.MyPlace2Learn.Controllers
{
    public class OrderController : Controller
    {
        private LearningContext db = new LearningContext();

        //
        // GET: /Order/5
        [Authorize]
        [LayoutSetter("_LayoutNoLeftMenu")]
        public ActionResult Index(int? id, int? page)
        {
            IQueryable<Order> orders;
            if (id > 0)
            {
                orders = db.Orders.Where(u=>u.UserId == id && u.DSStatus == 6);
            }
            else
            {
                int currentId = Convert.ToInt32(HttpContext.Session["UserId"]);
                orders = db.Orders.Where(u => u.UserId == currentId && u.DSStatus == 6);
            }
            return View(orders.OrderBy(o=>o.OrderDate).ToPagedList(page ?? 1, 10));
        }

        //
        // GET: /Order/Details/5
        [Authorize]
        [LayoutSetter("_LayoutNoLeftMenu")]
        public ActionResult Details(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // GET: /Order/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Order/Create

        [HttpPost]
        public ActionResult Create(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(order);
        }

        //
        // GET: /Order/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // POST: /Order/Edit/5

        [HttpPost]
        public ActionResult Edit(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        //
        // GET: /Order/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // POST: /Order/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}