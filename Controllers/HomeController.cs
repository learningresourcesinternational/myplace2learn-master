﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;
using LRI.MyPlace2Learn.Attributes;

namespace LRI.MyPlace2Learn.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        LearningContext emeraldDB = new LearningContext();

        public ActionResult Index()
        {
            //var genres = emeraldDB.LearningCategories.Where(l => l.IsActive == true).ToList();
            return View(GetFeaturedResources());
        }


        public List<LearningResource> GetFeaturedResources()
        {
            return emeraldDB.LearningResources.Where(c => c.IsActive == true && c.IsFeatured == true).ToList();
        }

        [LayoutSetter("_LayoutNoLeftMenu")]
        public ActionResult Contact()
        {
            return View();
        }

        //
        // POST: Contact
        [HttpPost]
        public ActionResult Contact(ContactModel contact)
        {
            Utility.Email.SendMail(System.Configuration.ConfigurationManager.AppSettings["ContactUsEmail"], contact.Email, "From " + contact.Name + ":" + contact.Subject, contact.Message);
            return Redirect("ContactThanks");
        }

        public ActionResult ContactThanks()
        {
            return View();
        }

    }
}