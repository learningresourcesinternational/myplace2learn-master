﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;

namespace LRI.MyPlace2Learn.Controllers
{
    [Authorize(Roles = "LRIAdmin")]
    public class StoreManagerController : Controller
    {
        private LearningContext learningDB = new LearningContext();

        //
        // GET: /StoreManager/

        public ViewResult Index()
        {
            //var albums = db.Albums.Include(a => a.Genre).Include(a => a.Artist);
            //return View(albums.ToList());

            var learningResouces = learningDB.LearningResources.Include(lr => lr.LearningCategories);
            return View(learningResouces.ToList());

        }

        //
        // GET: /StoreManager/Details/5

        public ViewResult Details(int id)
        {
            LearningResource learningResource = learningDB.LearningResources.Find(id);
            return View(learningResource);
        }

        //
        // GET: /StoreManager/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /StoreManager/Create

        [HttpPost]
        public ActionResult Create(LearningResource learningresource)
        {
            if (ModelState.IsValid)
            {
                learningDB.LearningResources.Add(learningresource);
                learningDB.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(learningresource);
        }
        
        //
        // GET: /StoreManager/Edit/5
 
        public ActionResult Edit(int id)
        {
            LearningResource learningResource = learningDB.LearningResources.Find(id);
            return View(learningResource);
        }

        //
        // POST: /StoreManager/Edit/5

        [HttpPost]
        public ActionResult Edit(LearningResource learningResource)
        {
            if (ModelState.IsValid)
            {
                learningDB.Entry(learningResource).State = EntityState.Modified;
                learningDB.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(learningResource);
        }

        //
        // GET: /StoreManager/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View(learningDB.LearningResources.Find(id));
        }

        //
        // POST: /StoreManager/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            learningDB.LearningResources.Remove(learningDB.LearningResources.Find(id));
            learningDB.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            learningDB.Dispose();
            base.Dispose(disposing);
        }
    }
}