﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;
using LRI.MyPlace2Learn.ViewModels;
using LRI.MyPlace2Learn.Attributes;

namespace LRI.MyPlace2Learn.Controllers
{
    public class ShoppingCartController : Controller
    {
        LearningContext learningDB = new LearningContext();

        private decimal VAT = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["VAT"]) / 100;

        //
        // GET: /ShoppingCart/
        [LayoutSetter("_LayoutNoLeftMenu")]
        public ActionResult Index()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Set up our ViewModel
            var viewModel = new ShoppingCartViewModel
            {
                CartItems = cart.GetCartItems(),
                CartTotal = cart.GetTotal()
            };

            viewModel.VATTotal = viewModel.CartTotal * VAT;
            viewModel.OrderTotal = viewModel.CartTotal + viewModel.VATTotal;

            // Return the view
            return View(viewModel);
        }

        //
        // GET: /Store/AddToCart/5
        public ActionResult AddToCart(int id)
        {

            // Retrieve the resource from the database
            //LearningResource addedResource = learningDB.LearningResources
            //    .Single(r => r.LearningResourceID == id);

            IList<LearningResource> learningResourcesToAdd = learningDB.LearningResources
                                                             .Where(lr => lr.LearningResourceID == id || lr.ParentID == id)
                                                             .ToList();

            // Add it to the shopping cart
            var cart = ShoppingCart.GetCart(this.HttpContext);
            
            foreach (LearningResource learningResource in learningResourcesToAdd)
            {
                cart.AddToCart(learningResource);
            }

            //learningDB.SaveChanges();

            // Go back to the main store page for more shopping
            return RedirectToAction("Index");
        }

        private bool openLearningPlanTaskExists(int userId, int learningResourceId)
        {
            return learningDB.LearningPlanTasks.Any(lpt => lpt.LearningResourceId == learningResourceId && lpt.UserId == userId && lpt.Finished == null && lpt.ExpiryDate >= System.DateTime.Now);        
        }

        //
        // AJAX: /ShoppingCart/RemoveFromCart/5
        [HttpPost]
        public JsonResult RemoveFromCart(int id)
        {
            // Remove the item from the cart
            var cart = ShoppingCart.GetCart(this.HttpContext);
            string idsForRemoval = id.ToString();
            
            Cart cartItem = learningDB.Carts.Find(id);

            // Remove from cart
            int itemCount = cart.RemoveFromCart(id);

            //// does item have a children?
            IList<int> childIds = (learningDB.LearningResources
                                  .Where(l => l.ParentID == cartItem.LearningResourceId)
                                  .Select(l => l.LearningResourceID))
                                  .ToList();
            
           
            if (childIds.Count > 0)
            {
                 IList<int> itemsToBeDeleted = (learningDB.Carts
                                    .Where(c => childIds.Contains(c.LearningResourceId) && c.CartId == cartItem.CartId)
                                    .Select(i => i.RecordId))
                                    .ToList();

                foreach (int item in itemsToBeDeleted)
                {
                    itemCount = cart.RemoveFromCart(item);
                }
                
                itemsToBeDeleted.Add(id);

                idsForRemoval = string.Join(",", itemsToBeDeleted);

            }
            
            // Display the confirmation message
            var results = new ShoppingCartRemoveViewModel
            {
                Message = Server.HtmlEncode(cartItem.LearningResource.Title) +
                    " has been removed from your shopping cart.",
                CartTotal = cart.GetTotal(),
                CartCount = cart.GetCount(),
                ItemCount = itemCount,
                DeleteIds = idsForRemoval
            };

            results.VATTotal = Utility.UtilityFunction.VATCalculation(results.CartTotal);

            return Json(results);
        }

        //
        // GET: /ShoppingCart/CartSummary
        [ChildActionOnly]
        public ActionResult CartSummary()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            ViewData["CartCount"] = cart.GetCount();

            return PartialView("CartSummary");
        }
    }
}