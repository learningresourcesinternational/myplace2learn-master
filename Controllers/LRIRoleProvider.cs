﻿#region namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#endregion

#region Project / specific namespace references
using SessionEntity = System.Web.HttpContext;
#endregion




namespace LRI.MyPlace2Learn.Controllers
{
    public class LRIRoleProvider : System.Web.Security.RoleProvider
    {
        #region properties
        public override string ApplicationName { get; set; }
        #endregion


        private LRI.MyPlace2Learn.Models.LearningContext learningDB = new LRI.MyPlace2Learn.Models.LearningContext();

        #region public overridden methods
        /// <summary>
        /// Initiliases the Role Provider
        /// </summary>
        /// <param name="name">the "friendly" name of the role provider</param>
        /// <param name="config">Collection of name value pairs from the provider</param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "LRIRoleProvider";
            }
            ApplicationName = "ELEarning";

            base.Initialize(name, config);
        }

        /// <summary>
        /// Retrieves a list of roles that the user is assigned to
        /// </summary>
        /// <param name="userId">Id of the user whose roles you want a list of</param>
        /// <returns>String array containing a list of roles</returns>
        public override string[] GetRolesForUser(string userId)
        {
            //if (SessionEntity.Current.Session != null && SessionEntity.Current.Session["UserRoles"] != null && SessionEntity.Current.Session["UserRoles"].ToString().Length > 0)
            //{
            //    return (SessionEntity.Current.Session["UserRoles"].ToString()).Split(Convert.ToChar(","));
            //}
            //else
            //{
            // IQueryable<RoleEntity> roles = (new UserBusiness()).GetUserRoles(Convert.ToInt32(SessionEntity.Current.Session["UserID"]));

            //    List<string> roleNames = new List<string>();
            //    foreach (RoleEntity role in roles)
            //    {
            //        roleNames.Add(role.Name);
            //    }
                    
            //    SessionEntity.Current.Session["UserRoles"] = string.Join(",", roleNames);
            //    return roleNames.ToArray();
            //}


            if (SessionEntity.Current.Session != null)
            {
                int currentUser = Convert.ToInt32(SessionEntity.Current.Session["UserID"]);

                List<string> roleNames = (from r in learningDB.Roles.Include("Users")
                                          .Where(u => u.Users.FirstOrDefault().UserId == currentUser)
                                          select r.Name).ToList();

                return roleNames.ToArray();
                                        
            }
            return null;
        }

        /// <summary>
        /// Checks to see if the user is a particular role
        /// </summary>
        /// <param name="username">Id of the user that you are checking</param>
        /// <param name="roleName">Name of the role you want to check for</param>
        /// <returns>True if user is found to be in the role, false otherwise</returns>
        public override bool IsUserInRole(string username, string roleName)
        {
            return (bool) GetRolesForUser(username).Contains(roleName);
        }

        #endregion

        #region inherited methods not implemented

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }



        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }


        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
