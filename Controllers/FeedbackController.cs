﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LRI.MyPlace2Learn.Models;

namespace LRI.MyPlace2Learn.Controllers
{
    public class FeedbackController : Controller
    {
        private LearningContext learningDB = new LearningContext();

        //
        // GET: /Feedback/

        public ActionResult Index()
        {
            return View(learningDB.Feedbacks.ToList());
        }

        //
        // GET: /Feedback/Details/5

        public ActionResult Details(int id = 0)
        {
            Feedback feedback = learningDB.Feedbacks.Find(id);
            if (feedback == null)
            {
                return HttpNotFound();
            }
            return View(feedback);
        }
        

        //
        // GET: /Feedback/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Feedback/Create

        [HttpPost]
        public ActionResult Create(Feedback feedback)
        {
            if (ModelState.IsValid)
            {
                feedback.DateCreated = DateTime.Now;

                learningDB.Feedbacks.Add(feedback);
                learningDB.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(feedback);
        }

        //
        // GET: /Feedback/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Feedback feedback = learningDB.Feedbacks.Find(id);
            if (feedback == null)
            {
                return HttpNotFound();
            }
            return View(feedback);
        }

             //
        // AJAX: /LearningPlan/PostFeedback/
        [HttpPost]
        public bool PostFeedback(FormCollection values)
        {            
            int currentUser = Convert.ToInt32(HttpContext.Session["UserId"]);
            Feedback newFeedback = new Feedback();

            try
            {
                newFeedback.Rating = Convert.ToInt32(values["rating"]);
                newFeedback.Comments = values["comment"];
                newFeedback.UserId = currentUser;
                newFeedback.DateCreated = System.DateTime.Now;
                newFeedback.IsActive = true;
                newFeedback.LearningResourceId = Convert.ToInt32(values["lrid"]);               

                learningDB.Feedbacks.Add(newFeedback);
                learningDB.SaveChanges();
            }
            catch (SystemException err)
            {
                Utility.ErrorHandling.FeedbackError(newFeedback, err);
                return false;
            }

            var userposting =   learningDB.Users
                                .Where(su=>su.UserId == currentUser)
                                .Select(su=>new{su.FirstName, su.LastName}).FirstOrDefault();

            Utility.Email.SendMail("FEEDBACKNOTE", newFeedback, values["title"], userposting.FirstName + " " + userposting.LastName);
            return true;        
        }

        protected override void Dispose(bool disposing)
        {
            learningDB.Dispose();
            base.Dispose(disposing);
        }
    }
}