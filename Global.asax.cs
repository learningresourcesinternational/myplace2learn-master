﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
//using RusticiSoftware.HostedEngine.Client;
using System.Web.Configuration;
using System.Web.Security;

namespace LRI.MyPlace2Learn
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            //routes.MapRoute("GetLaunchUrl", "{controller}/{action}/{lptid}",
            //                new { controller = "LearningPlan", action = "GetLaunchUrl" }
            //                );
            routes.MapRoute("ResetRegistration", "{Controller}/{Action}/{id}", new { controller = "LearningPlan", action = "ResetRegistration" });
            routes.MapRoute("CompletePayment", "{Controller}/{Action}/{id}/{token}", new { controller = "Checkout", action = "CompletePayment" });
            routes.MapRoute("Browse", "{Controller}/{Action}/{catid}/{page}/{searchTerm}", new { controller = "Store", action = "Browse", catid = UrlParameter.Optional, page = UrlParameter.Optional, searchTerm = UrlParameter.Optional});
           
        }

        protected void Application_Start()
        {
            System.Data.Entity.Database.SetInitializer<LRI.MyPlace2Learn.Models.LearningContext>(null);
			

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);


			//ScormCloud.Configuration =  new RusticiSoftware.HostedEngine.Client.Configuration(
			//							WebConfigurationManager.AppSettings["HostedEngineWebServicesUrl"],
			//							WebConfigurationManager.AppSettings["HostedEngineAppId"],
			//							WebConfigurationManager.AppSettings["HostedEngineSecurityKey"],
			//							WebConfigurationManager.AppSettings["Origin"]);


            BootstrapSupport.BootstrapBundleConfig.RegisterBundles(System.Web.Optimization.BundleTable.Bundles);
            //BootstrapMvcSample.ExampleLayoutsRouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                //old authentication, kill it
                //FormsAuthentication.SignOut();
                //HttpContext.Current.Response.End();
            }
        }
    }
}