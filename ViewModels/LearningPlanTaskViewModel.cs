﻿using System.Collections.Generic;
using LRI.MyPlace2Learn.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LRI.MyPlace2Learn.ViewModels
{
    public class LearningPlanTaskViewModel
    {
        public string ResourceTitle { get; set; }
        public string LearningTime { get; set; }
        public string Started { get; set; }
        public string Completed { get; set; }
        public bool IsScored { get; set; }
        public bool HasTest { get; set; }
        public bool HasExpired { get; set; }

    }
}