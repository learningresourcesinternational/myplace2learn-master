﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Linq;

namespace LRI.MyPlace2Learn.ViewModels
{
    public class CatalogueMenuViewModel
    {
        public int CategoryID { get; set; }
        public string Name { get; set; }
        public virtual IList<LRI.MyPlace2Learn.ViewModels.CatalogueMenuViewModel> ChildCategories { get; set; }





    }
}

