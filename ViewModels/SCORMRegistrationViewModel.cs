﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Linq;

namespace LRI.MyPlace2Learn.ViewModels
{
    public class SCORMRegistrationViewModel
    {
        public string RegistrationId { get; set; }
        public string CourseId { get; set; }
        public string CourseTitle { get; set; }
        public string LearnerId { get; set; }
        public string LearnerName { get; set; }
    }
}

