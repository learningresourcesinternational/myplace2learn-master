﻿using System.Collections.Generic;
using LRI.MyPlace2Learn.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LRI.MyPlace2Learn.ViewModels
{
    public class ScoresViewModel
    {
        public decimal Score { get; set; }
        public string Pass { get; set; }
        public string DateAcheived { get; set; }
    }
}