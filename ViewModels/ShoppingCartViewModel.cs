﻿extern alias DataAnnotations;
using System.Collections.Generic;
using LRI.MyPlace2Learn.Models;
using System.ComponentModel;
using DataAnnotations.System.ComponentModel.DataAnnotations;

namespace LRI.MyPlace2Learn.ViewModels
{
    public class ShoppingCartViewModel
    {
        public List<Cart> CartItems { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0.00}")]
        public decimal CartTotal { get; set; }
        public decimal VATTotal { get; set; }
        public decimal OrderTotal { get; set; }
    }
}