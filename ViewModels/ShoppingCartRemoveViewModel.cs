﻿namespace LRI.MyPlace2Learn.ViewModels
{
    public class ShoppingCartRemoveViewModel
    {
        public string Message { get; set; }
        public decimal CartTotal { get; set; }
        public int CartCount { get; set; }
        public int ItemCount { get; set; }
        public string DeleteIds { get; set; }
        public decimal VATTotal { get; set; }
        
    }
}