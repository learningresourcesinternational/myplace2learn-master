﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace LRI.MyPlace2Learn.Models
{
    public class LearningContext : DbContext
    {
       

        public DbSet<LearningCategory> LearningCategories { get; set; }
        public DbSet<LearningResource> LearningResources { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<LearningPlanTask> LearningPlanTasks { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<Dataset> DataSets { get; set; }
        public DbSet<DatasetValue> DatasetValues { get; set; }
        public DbSet<SystemError> SystemErrors { get; set; }
        public DbSet<OnlineCourseScore> OnlineCourseScores { get; set; }
        public DbSet<NewsletterSubscription> NewsletterSubscriptions { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }


        public LearningContext()
            : base("LearningConn")
        { 
            
        }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // creates mapping table in model - we need this because we are mapping to an existing table which does not 
            // follow the assumed model conventions
            modelBuilder.Entity<LearningCategory>()
                .HasMany(l => l.LearningResources)
                .WithMany(c => c.LearningCategories)
                .Map(mc =>
                    {
                        mc.ToTable("LearningResourceCategory");
                        mc.MapLeftKey("LearningCategoryID");
                        mc.MapRightKey("LearningResourceID");
                    });


            modelBuilder.Entity<Role>()
                   .HasMany(u => u.Users)
                   .WithMany(c => c.Roles)
                   .Map(mc =>
                   {
                       mc.ToTable("UsersRole");
                       mc.MapLeftKey("RoleId");
                       mc.MapRightKey("UserId");
                   });

        }

        
    }
}
