﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LRI.MyPlace2Learn.DAL.Repositories
{
    interface IStoreRepository
    {
        IList<MyPlace2Learn.ViewModels.CatalogueMenuViewModel> GetMenu();
    }
}
