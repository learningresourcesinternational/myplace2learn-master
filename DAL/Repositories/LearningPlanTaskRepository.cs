﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LRI.MyPlace2Learn.Models;
//using LRI.MyPlace2Learn.BusinessLayer;

namespace LRI.MyPlace2Learn.DAL.Repositories
{
    public class LearningPlanTaskRepository : ILearningPlanTaskRepository
    {
        private readonly LearningContext context = new LearningContext();

        #region Record Retrieval
        /// <summary>
        /// Returns the basics of a query pertaining to learning plan tasks
        /// </summary>
        /// <param name="userId">User id of the user you want the learning plan tasks for</param>
        /// <returns>IQueryable for set of LearningPlanTasks</returns>
        public IQueryable<LearningPlanTask> ListLearningPlanTasks(int userId)
        {
            var learningplantasks = from lp in context.LearningPlanTasks
                                    join lr in context.LearningResources on lp.LearningResourceId equals lr.LearningResourceID
                                    join f in context.Feedbacks on new { key1 = lp.LearningResourceId, key2 = lp.UserId } equals new { key1 = f.LearningResourceId, key2 = f.UserId } into uf
                                    where lp.UserId == userId
                                    from f in uf.DefaultIfEmpty()
                                    select lp;

            return learningplantasks.OrderByDescending(l => l.ExpiryDate);
        }

        /// <summary>
        /// Get a learning plan task for specified id
        /// </summary>
        /// <param name="learningPlanTaskId">Id of the learning plan task you want</param>
        /// <returns>LEarning plan task as specified by id</returns>
        public LearningPlanTask GetLearningPlanTask(int learningPlanTaskId)
        {
            return context.LearningPlanTasks.SingleOrDefault(lpt => lpt.LearningPlanTaskId == learningPlanTaskId);
        }

        /// <summary>
        /// Gets the learning resource id from learning plan task record
        /// </summary>
        /// <param name="learningPlanTaskId">Learning Plan Task id of the learning plan task record we want the learning resource id</param>
        /// <returns>Learning resource id of learning plan task for the id passed in</returns>
        public int GetLearningResourceId(int learningPlanTaskId)
        {
            return (from lpt in context.LearningPlanTasks
                    where lpt.LearningPlanTaskId == learningPlanTaskId
                    select lpt.LearningResourceId).SingleOrDefault();
        }

       

        #endregion

        #region Records editing
  
        /// <summary>
        /// Updates a learning plan task
        /// </summary>
        /// <param name="learningPlanTask">Learning plan task to update</param>
        public void UpdateLearningPlanTask(LearningPlanTask learningPlanTask)
        {
            context.Entry(learningPlanTask).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }
        #endregion

        public IList<ViewModels.SCORMRegistrationViewModel> GetRegistrationsList()
        {
            //List<RegistrationData> registrationData = getRegistrationsListFromSCORM().ToList();
            List<ViewModels.SCORMRegistrationViewModel> registrationViewModels = new List<ViewModels.SCORMRegistrationViewModel>();


			//foreach (RegistrationData regData in registrationData)
			//{
			//	registrationViewModels.Add(new ViewModels.SCORMRegistrationViewModel
			//	{
			//		RegistrationId = regData.RegistrationId,
			//		LearnerId = regData.LearnerId,
			//		LearnerName = regData.LearnerName,
			//		CourseId = regData.CourseId,
			//		CourseTitle = regData.CourseTitle
			//	});
			//}

            return registrationViewModels;
        }


		//private IList<RegistrationData> getRegistrationsListFromSCORM()
		//{
		//	return ScormCloud.RegistrationService.GetRegistrationList();
		//}





    }
}