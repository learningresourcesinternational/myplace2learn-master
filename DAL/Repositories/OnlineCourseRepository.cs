﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LRI.MyPlace2Learn.Models;
using LRI.MyPlace2Learn.ViewModels;
//using RusticiSoftware.HostedEngine.Client;
//using LRI.MyPlace2Learn.BusinessLayer;

namespace LRI.MyPlace2Learn.DAL.Repositories
{
    public class OnlineCourseScoreRepository : IOnlineCourseScoreRepository
    {
        private readonly LearningContext context = new LearningContext();

        #region Record Retrieval
        public IList<OnlineCourseScore> ListOnlineCourseScores(int userId)
        {
           return context.OnlineCourseScores.Where(ocs => ocs.LearningPlanTaskId == userId).OrderByDescending(ocs => ocs.AddedOn).ToList();            
        }
        #endregion

        #region Records editing
        /// <summary>
        /// Creates a new online course score record
        /// </summary>
        /// <param name="onlineCourseScore">Online course score to add</param>
        public void CreateOnlineCourseScore(OnlineCourseScore onlineCourseScore)
        {
            context.OnlineCourseScores.Add(onlineCourseScore);
            context.SaveChanges();
        }
        #endregion
    }
}