﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LRI.MyPlace2Learn.Models;

namespace LRI.MyPlace2Learn.DAL.Repositories
{
    interface IOnlineCourseScoreRepository
    {
        IList<OnlineCourseScore> ListOnlineCourseScores(int userId);
        void CreateOnlineCourseScore(OnlineCourseScore onlineCourseScore); 
    }
}
