﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LRI.MyPlace2Learn.Models;
//using RusticiSoftware.HostedEngine.Client;


namespace LRI.MyPlace2Learn.DAL.Repositories
{
    public interface ILearningPlanTaskRepository
    {
        IQueryable<LearningPlanTask> ListLearningPlanTasks(int userId);
        LearningPlanTask GetLearningPlanTask(int id);
        void UpdateLearningPlanTask(LearningPlanTask learningPlanTask);
        IList<ViewModels.SCORMRegistrationViewModel> GetRegistrationsList();
    }
}