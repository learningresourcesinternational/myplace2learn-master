﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LRI.MyPlace2Learn.Models;

namespace LRI.MyPlace2Learn.DAL.Repositories
{
    public class StoreRepository : IStoreRepository
    {
        private readonly LearningContext context = new LearningContext();
        
        public IList<ViewModels.CatalogueMenuViewModel> GetMenu()
        {
            IList<LearningCategory> categories = context.LearningCategories.Where(l => l.IsActive == true).ToList();
            IList<ViewModels.CatalogueMenuViewModel> learningCategoryMenu = (from c in categories
                                                                  where c.ParentLearningCategoryID == null || c.ParentLearningCategoryID == 0
                                                                             select new ViewModels.CatalogueMenuViewModel
                                                                  {
                                                                      CategoryID = c.LearningCategoryID,
                                                                      Name = c.Name
                                                                  }).ToList();

            foreach (ViewModels.CatalogueMenuViewModel item in learningCategoryMenu)
            {
                if (categories.Count(c=>c.ParentLearningCategoryID == item.CategoryID) > 0)
                { 
                    item.ChildCategories = setUpChildren(item.CategoryID, categories);
                    foreach (ViewModels.CatalogueMenuViewModel category in item.ChildCategories)
                    {
                        category.ChildCategories = setUpChildren(category.CategoryID, categories);
                    }
                }
            }

            return learningCategoryMenu;
        }

        private IList<ViewModels.CatalogueMenuViewModel> setUpChildren(int categoryID, IList<LearningCategory> categories)
        {
            return (from c in categories
                    where c.ParentLearningCategoryID == categoryID
                    select new ViewModels.CatalogueMenuViewModel
                    {
                        CategoryID = c.LearningCategoryID,
                        Name = c.Name
                    }).ToList();
        }


    }
}