﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LRI.MyPlace2Learn.Models;

namespace LRI.MyPlace2Learn.Helpers
{
    /// <summary>
    /// A list of dataset values
    /// </summary>
    public class DataSetValueList
    {

        private IList<DatasetValue> datasetValues;

        /// <summary>
        /// Constructor for datasetvaluelist
        /// </summary>
        /// <param name="dsId">Dataset id you want the list for</param>
        public DataSetValueList(int dsId)
        {
            datasetValues = (new LearningContext()).DatasetValues.Where(dv => dv.IsActive == true && dv.DatasetId == dsId).OrderBy(dv=>dv.Value).ToList();       
        }

        /// <summary>
        /// Gets a set of dataset values
        /// </summary>
        public IEnumerable<DatasetValue> DataSetValues
        {
            get { return datasetValues; }
        }

    }

    public class StatusListItems
    {
        private IList<System.Web.Mvc.SelectListItem> statusListItems = new List<System.Web.Mvc.SelectListItem>();

        public IEnumerable<System.Web.Mvc.SelectListItem> ListItems { get { return statusListItems; } }

        public StatusListItems(bool status)
        {
            statusListItems.Add(new System.Web.Mvc.SelectListItem { Value = "1", Text = "Active", Selected = (status) ? true : false });
            statusListItems.Add(new System.Web.Mvc.SelectListItem { Value = "0", Text = "Archived", Selected = (!status) ? true : false });
        }

    }

    



}