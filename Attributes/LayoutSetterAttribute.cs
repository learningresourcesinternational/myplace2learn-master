﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LRI.MyPlace2Learn.Attributes
{
    public class LayoutSetterAttribute : ActionFilterAttribute
    {
        private readonly string masterLayout;

        public LayoutSetterAttribute(string layout)
        {
            masterLayout = layout;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            var result = filterContext.Result as ViewResult;
            if (result != null)
            {
                result.MasterName = masterLayout;
            }
        }
    }
}