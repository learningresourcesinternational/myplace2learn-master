﻿extern alias DataAnnotations;
using System;
using System.Collections.Generic;
using DataAnnotations.System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LRI.MyPlace2Learn.Attributes
{
    public class BooleanRequiredAttribute : ValidationAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            return (value != null)? true : false;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "booleanrequired"
            };
        }

    }
}