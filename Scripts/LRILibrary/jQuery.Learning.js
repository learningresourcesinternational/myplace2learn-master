﻿$(function () {

    var LRI = {

        DevPlan: {}

    };

    LRI.DevPlan.TaskData = {

        ID: 0,
        Type: "",
        ResourceId: 0,
        ResourceTitle: "",
        LearningTime: "",
        DateStarted: "",
        DateCompleted: "",
        IsScored: false,
        HasTest: false,
        expired: false

    }

    //$(".launchLink").click(function () {
        
    //    var learningResourceId = $(this).attr("cdata-id");
    //    $.post("/LearningPlan/GetLaunchUrl",
    //        {
    //            "lptid": learningPlanTaskId
    //        },
    //        function (data) {
    //            window.location = data;
    //        });
    //});



    LRI.DevPlan.TaskData.GetUrl = function () {
        //$.post("/LearningPlan/GetLaunchUrl",
        //    {
        //        "lptid": LRI.DevPlan.TaskData.ID,
        //    },
        //    function (data) {
        //        window.location = data;
    	//    });
    	window.location = "/CourseLaunch/Launch.aspx?TaskID=" + LRI.DevPlan.TaskData.ID;
    }

    $(".viewLink").click(function () {
        LRI.DevPlan.TaskData.ID = $(this).attr("data-id");
        LRI.DevPlan.TaskData.ResourceId = $(this).attr("cdata-id");
        LRI.DevPlan.TaskData.Type = "OLC";

        $("#happyDialog").dialog({
            title: 'loading....'
        });

        $("#TaskDetails").dialog({
            modal: true,
            title: "Course Details",
            height: 430,
            width: 470,
            resizable: false,
            buttons: {
                Launch: function () {
                    LRI.DevPlan.TaskData.GetUrl();
                },
                "Course Details": function () {

                    LRI.DevPlan.TaskData.Type = "OLC";
                    LRI.DevPlan.SetTaskInfo();

                },
                "Score History": function () {
                    LRI.DevPlan.TaskData.Type = "SH";
                    LRI.DevPlan.SetTaskInfo();
                },
                Certificate: function () {
                    window.open("/LearningPlan/Certificate/" + LRI.DevPlan.TaskData.ID, "CoursewareHolder", "height=600,width=800");
                    $(this).dialog("close");
                },
                Close: function () {
                    var url = [location.protocol, '//', location.host, location.pathname].join('');
                    window.location = url;
                }
            }
        });

        LRI.DevPlan.SetTaskInfo();

    });



    LRI.DevPlan.SetTaskInfo = function () {
        // Get Buttons...
        var dlgBasicButton = $(".ui-dialog-buttonpane").find("button:contains('Course Details')");
        var dlgScoreButton = $(".ui-dialog-buttonpane").find("button:contains('Score History')");

        // Hide everything...
        $("#TaskDetails div").each(function () {
            $(this).hide();
        });

        // hide all buttons
        $(".ui-dialog-buttonpane button").each(function () {

            if ($(this).text() != "Close") {
                $(this).hide();
            }
        });

        $("#loader").show();

        switch (LRI.DevPlan.TaskData.Type) {
        	case "OLC":
        		LRI.DevPlan.LoadContent(function () {
        			LRI.DevPlan.DisplayBasic();
        		});                
                $("#Basic").show();
                break;
            case "SH":
                dlgScoreButton.hide();
                dlgBasicButton.show();
                LRI.DevPlan.LoadContent();
                $("#ScoreHistory").show();               
                break;
        }
        $("#loader").hide();
    };

    LRI.DevPlan.DisplayBasic = function () {

        var html = "";        

        $("#lblDlgName").text(LRI.DevPlan.TaskData.ResourceTitle);
        $("#lblDlgLearningTime").text(LRI.DevPlan.TaskData.LearningTime);
        $("#lblDlgStartDate").text(LRI.DevPlan.TaskData.DateStarted);
        $("#lblDlgFinishDate").text(LRI.DevPlan.TaskData.DateCompleted);

        var dlgLaunchButton = $(".ui-dialog-buttonpane").find("button:contains('Launch')");
        var dlgScoreButton = $(".ui-dialog-buttonpane").find("button:contains('Score History')");
        var isComplete = true;
               
        // if(LRI.DevPlan.TaskData.Type == "OLC")
        if (!LRI.DevPlan.TaskData.Expired) {
            dlgLaunchButton.show();
        }
        else
        {
            dlgLaunchButton.hide();
        }

        if (LRI.DevPlan.TaskData.DateCompleted == "" || LRI.DevPlan.TaskData.DateCompleted == "In progress" || LRI.DevPlan.TaskData.DateCompleted == "n/a") {
            isComplete = false;
        }


        if (LRI.DevPlan.TaskData.IsScored) {
            dlgScoreButton.show();
        }

        // Check if we need to display the Certificate button...

        if ((LRI.DevPlan.TaskData.Scored && isComplete) || (!LRI.DevPlan.TaskData.Scored && !LRI.DevPlan.TaskData.HasTest && isComplete)) {
            $(".ui-dialog-buttonpane").find("button:contains('Certificate')").show();
        }
    }

    LRI.DevPlan.LoadContent = function (callback) {
        var json = "";
        switch (LRI.DevPlan.TaskData.Type) {
            case "OLC":
                $.post("/LearningPlan/GetLearningPlanTaskDetails",
                    {
                        "ID": LRI.DevPlan.TaskData.ID
                    },
                    function (data) {
                        if (data != null) {
                            LRI.DevPlan.TaskData.ResourceTitle = data.ResourceTitle;
                            LRI.DevPlan.TaskData.LearningTime = data.LearningTime;
                            LRI.DevPlan.TaskData.DateStarted = data.Started;
                            LRI.DevPlan.TaskData.DateCompleted = data.Completed;
                            LRI.DevPlan.TaskData.IsScored = data.IsScored;
                            LRI.DevPlan.TaskData.HasTest = data.HasTest
                            LRI.DevPlan.TaskData.Expired = data.HasExpired;
                            callback();
                        }

                    });
                break;
            case "SH":
                $.post("/LearningPlan/GetScores",
                        {
                            "ID": LRI.DevPlan.TaskData.ID
                        },
                        function (data) {
                            if (data.length > 0) {
                                if (data != "TIMEDOUT") {
                                    LRI.DevPlan.DisplayScores(data);
                                }
                                else {
                                    document.location = "/Account/Login/";
                                }
                            }
                        });
                break;

        }
    }

    LRI.DevPlan.DisplayScores = function (json) {
        var html = "";

        // Remove all scores...
        $("tr[data-type='dynamic']").remove();

        if (json != null) {

            for (var i = 0; i < json.length; i++) {
                //alert(json[i].Attempt);
                html += "<tr data-type='dynamic'><td>" + (i + 1) + "</td><td>" + json[i].Score + "</td><td>" + json[i].Pass + "</td><td>" + json[i].DateAcheived + "</td></tr>";
            }

        } else {

            html = "<tr data-type='dynamic'><td colspan='4' style='text-align: center'>- There have been no attempts for this task -</td></tr>";

        }

        $("#Scores tr:last").after(html);

    }

    $(".reviewLink").click(function () {
        var lrId = $(this).attr("data-id");
        var title = $(this).attr("data-title");
        $("#FeedbackForm").dialog({
            modal: true,
            title: "Review " + title,
            height: 430,
            width: 470,
            resizable: false,
            buttons: {
                Submit: function () {
                    if (validateRating()) {
                        postFeedback(lrId, title);
                        alert("Thank you for your review.");
                        location.reload();
                    }
                },
                Cancel: function () {
                    var url = [location.protocol, '//', location.host, location.pathname].join('');
                    window.location = url;
                }
            }
        });
    });

    $(".starRating").click(function () {
        var rating = $(this).attr("data-value");
        if ($("#Rating").attr("value") > 0) {
            for (i = $("#Rating").attr("value"); i > 0; i--) {
                $("#img" + i).attr("src", "../../Content/Images/icons/star-off.png");
            }
        }

        $("#Rating").val(rating);

        for (var i = 1; i <= rating; i++) {
            $("#img" + i).attr("src", "../../Content/Images/icons/star-on.png");
        }
    });

    function validateRating() {
        var message = "";
        if ($("#Rating").attr("value") == 0 || $.trim($("#feedbackComments").val()) == "") {
            alert("Please ensure that you have given the course a rating and entered your comments in the review box.");
            return false;
        }
        return true;
    }

    function postFeedback(learningResourceId, title) {
        $.post("/FeedBack/PostFeedback",
        {
            "lrid": learningResourceId,
            "comment": $("#feedbackComments").val(),
            "rating": $("#Rating").val(),
            "title": title
        },
        function (data) {
            return data;
        })
    }
});

